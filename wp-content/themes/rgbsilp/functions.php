<?php
//-----------------------------------  // Load Scripts & Styles //-----------------------------------//
function vesst_scripts_styles() {
	//---------------------  // Stylesheets // ---------------------//	
	wp_enqueue_style( 'vesst_css', get_template_directory_uri() . "/includes/css/rgbsi.css" );		
	//---------------------  // Scripts // ---------------------//
	wp_enqueue_script('jquery');
	wp_enqueue_script('theme_custom_js', get_template_directory_uri() . '/includes/js/theme-min.js', false, false, false);
}
add_action( 'wp_enqueue_scripts', 'vesst_scripts_styles' );
//-----------------------------------  // Required Files  //-----------------------------------//
// MUST HAVE
require get_template_directory() . '/admin/theme-support/vesst-theme-support.php';
require get_template_directory() . '/admin/theme-support/vesst-menus.php';
require get_template_directory() . '/admin/theme-support/vesst-images.php';
require get_template_directory() . '/admin/theme-support/vesst-sidebars.php';

// TURN OFF / ON
// require get_template_directory() . '/admin/theme-support/vesst-breadcrumbs.php';
// require get_template_directory() . '/admin/theme-support/vesst-pagination.php';
// require get_template_directory() . '/admin/theme-support/vesst-excerpt.php';

// ACF ON
require get_template_directory() . '/admin/acf/acf.php';
require get_template_directory() . '/admin/acf/acf-options.php';
require get_template_directory() . '/admin/acf/acf-gravity-forms.php';

flush_rewrite_rules( false );
require get_template_directory() . '/admin/post-type/post-type-resources.php';
// require get_template_directory() . '/admin/post-type/post-type-events.php';
// require get_template_directory() . '/admin/post-type/post-type-team.php';


//-----------------------------------  // Register Blocks ACF RENDER //-----------------------------------//

function my_acf_block_render_callback( $block ) {
		
		// convert name ("acf/testimonial") into path friendly slug ("testimonial")
		$slug = str_replace('acf/', '', $block['name']);
		
		// include a template part from within the "includes/blocks" folder
		if( file_exists( get_theme_file_path("/includes/blocks/content-{$slug}.php") ) ) {
				include( get_theme_file_path("/includes/blocks/content-{$slug}.php") );
		}
}



// * Loading editor styles for the block editor (Gutenberg)
// add_action('enqueue_block_editor_assets','add_block_editor_assets',10,0);
// function add_block_editor_assets(){
// 	wp_enqueue_style('block_editor_css', get_theme_file_uri( '/includes/css/gutenberg.css' ));
// }

// * Set Color Pallet(ACF)
function acf_input_admin_footer() { ?>
	<script type="text/javascript">
	(function($) {
	acf.add_filter('color_picker_args', function( args, $field ){
	args.palettes = ['#008ebd', '#071E42', '#2a3462' , '#7235F4', '#F22F46' ]
	return args;
	});
	 
	})(jQuery);
	</script>
	<?php
	 
}
	
add_action('acf/input/admin_footer', 'acf_input_admin_footer');


// Changing excerpt more
   function new_excerpt_more($more) {
   global $post;
//    return '… <a href="'. get_permalink($post->ID) . '" class="read_more">' . 'Read More' . '</a>';
   return '...';
   }
   add_filter('excerpt_more', 'new_excerpt_more');

/**
 * Filter the excerpt length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function my_acf_google_map_api( $api ){
	// DEVELOPMENT KEY TODO CHANGE
	$api['key'] = 'AIzaSyDgqaCX1djgv_lg9vSJn6lOheDODS1ULEM';
	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

//-----------------------------------  // Write Log  //-----------------------------------//

if ( ! function_exists('write_log')) {
	function write_log ( $log )  {
	   if ( is_array( $log ) || is_object( $log ) ) {
		  error_log( print_r( $log, true ) );
	   } else {
		  error_log( $log );
	   }
	}
}


function ea_setup() {
	// Disable Custom Colors
	add_theme_support( 'disable-custom-colors' );
  
	// Editor Color Palette
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'Blue', 'ea-starter' ),
			'slug'  => 'blue',
			'color'	=> '#008EBD',
		),
		array(
			'name'  => __( 'Dark Blue', 'ea-starter' ),
			'slug'  => 'dkblue',
			'color' => '#071E42',
		),
		array(
			'name'  => __( 'Purple', 'ea-starter' ),
			'slug'  => 'purple',
			'color' => '#7235F4',
		),
		array(
			'name'	=> __( 'Red', 'ea-starter' ),
			'slug'	=> 'red',
			'color'	=> '#F22F46',
		),
		array(
			'name'	=> __( 'White', 'ea-starter' ),
			'slug'	=> 'white',
			'color'	=> '#ffffff',
		),
	) );
}
add_action( 'after_setup_theme', 'ea_setup' );

function truncate($string,$length=20,$append="&hellip;") {
	$string = trim($string);
  
	if(strlen($string) > $length) {
	  $string = wordwrap($string, $length);
	  $string = explode("\n", $string, 2);
	  $string = $string[0] . $append;
	}
  
	return $string;
}



add_filter( 'gform_submit_button', 'vesst_add_span_tags', 10, 2 );
function vesst_add_span_tags ( $button, $forms ) {

return $button .= "<span aria-hidden='true'></span>";

}