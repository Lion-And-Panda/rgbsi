<style type="text/css">
.acf-map {
    width: 100%;
    height: 900px;
    margin: 0;
}
.gm-control-active.gm-fullscreen-control{
 display:none;
}
.acf-map img {
   max-width: inherit !important;
}

    /*style the box*/  
.gm-style .gm-style-iw {
    background-color: #202E46 !important;
    top: 0 !important;
    left: 0 !important;
    width: 100% !important;
    height: 100% !important;
    min-height: 120px !important;
    padding-top: 10px;
    display: block !important;
    border-radius: 0px !important;
    height: 100%;
}  
.gm-style-iw-d h3 {
    font-size: 2em;
    font-weight: 800;
}
.gm-style .gm-style-iw-d::-webkit-scrollbar {
    display:none;
}
.gm-style .gm-style-iw-t::after {
    background: linear-gradient(45deg,rgba(32,46,70,1.0) 50%,rgba(32,46,70,0) 51%,rgba(32,46,70,0) 100%);
}  

    /*style the p tag*/
.gm-style .gm-style-iw #google-popup p{
    padding: 10px;
}
    

/*style the arrow*/
.gm-style div div div div div div div div {
    background-color: #202E46 !important;
    padding: 0;
    margin: 0;
    padding: 0;
    top: 0;
    color: #fff;
    font-size: 16px;
}

/*style the link*/
.gm-style div div div div div div div div a {
    color: #f1f1f1;
    font-weight: bold;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgqaCX1djgv_lg9vSJn6lOheDODS1ULEM"></script>
<script type="text/javascript">
(function( $ ) {

/**
 * initMap
 *
 * Renders a Google Map onto the selected jQuery element
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   jQuery $el The jQuery element.
 * @return  object The map instance.
 */
function initMap( $el ) {

    // Find marker elements within map.
    var $markers = $el.find('.marker');

    // Create gerenic map.
    var mapArgs = {
        zoom            : 5,
        center          : new google.maps.LatLng(0, 0),
        mapTypeControl  : false,
        panControl      : false,
        scrollwheel     : false,
        zoomControl     : false,
		navigationControl	: false,
		mapTypeControl	: false,
		scaleControl	: false,
		//draggable	    : false,
        streetViewControl: false,
        fullScreenControl: false,
        mapTypeId       : google.maps.MapTypeId.ROADMAP,
        styles          : [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#2d3c52"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#2d3c52"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": "-100"
            },
            {
                "visibility": "on"
            },
            {
                "saturation": "0"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }
]
    };
    var map = new google.maps.Map( $el[0], mapArgs );

    // Add markers.
    map.markers = [];
    $markers.each(function(){
        initMarker( $(this), map );
    });

    // Center map based on markers.
    centerMap( map );

    // Return map instance.
    return map;
}

/**
 * initMarker
 *
 * Creates a marker for the given jQuery element and map.
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   jQuery $el The jQuery element.
 * @param   object The map instance.
 * @return  object The marker instance.
 */
function initMarker( $marker, map ) {

    // Get position from marker.
    var lat = $marker.data('lat');
    var lng = $marker.data('lng');
    var latLng = {
        lat: parseFloat( lat ),
        lng: parseFloat( lng )
    };

    var icon = {
        url: '<?php echo get_template_directory_uri(); ?>/images/svg/map.svg',
        scaledSize: new google.maps.Size(22, 22), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };


    // Create marker instance.
    var marker = new google.maps.Marker({
        position : latLng,
        map: map,
        icon:  icon
    });

    // Append to reference for later use.
    map.markers.push( marker );

    // If marker contains HTML, add it to an infoWindow.
    if( $marker.html() ){

        // Create info window.
        var infowindow = new google.maps.InfoWindow({
            content: $marker.html()
        });

        // Show info window when marker is clicked.
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open( map, marker );
        });
    }
}

/**
 * centerMap
 *
 * Centers the map showing all markers in view.
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   object The map instance.
 * @return  void
 */
function centerMap( map ) {

    // Create map boundaries from all map markers.
    var bounds = new google.maps.LatLngBounds();
    map.markers.forEach(function( marker ){
        bounds.extend({
            lat: marker.position.lat(),
            lng: marker.position.lng()
        });
    });

    // Case: Single marker.
    if( map.markers.length == 1 ){
        map.setCenter( bounds.getCenter() );
		map.setZoom( 3 );

    // Case: Multiple markers.
    } else{
       // map.fitBounds( bounds );
        map.setCenter( bounds.getCenter() );
		map.setZoom( 3 );
    }
}

// Render maps on page load.
$(document).ready(function(){
    $('.acf-map').each(function(){
        var map = initMap( $(this) );
    });
});

})(jQuery);
</script>


<?php if( have_rows('maps') ): ?>
    <div class="acf-map group-1" data-zoom="5">
        <?php while ( have_rows('maps') ) : the_row(); 

            // Load sub field values.
            $location = get_sub_field('map_location');
            $title = get_sub_field('location_text');
            $description = get_sub_field('description');
            $group = get_sub_field('group');
            if($group == 1) {
            ?>
            <div  class="marker group-<?= $group; ?>" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>">
                <h3><?php echo esc_html( $title ); ?></h3>
                <p><em><?php echo esc_html( $location['address'] ); ?></em></p>
                <p><?php echo esc_html( $description ); ?></p>
            </div>
            <?php }
endwhile; ?>
    </div>
<?php endif; ?>

<?php if( have_rows('maps') ): ?>
    <div class="acf-map group-2" data-zoom="5" style="display: none">
        <?php while ( have_rows('maps') ) : the_row(); 

            // Load sub field values.
            $location = get_sub_field('map_location');
            $title = get_sub_field('location_text');
            $description = get_sub_field('description');
            $group = get_sub_field('group');
            if($group == 2) {
            ?>
            <div  class="marker " data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>">
                <h3><?php echo esc_html( $title ); ?></h3>
                <p><em><?php echo esc_html( $location['address'] ); ?></em></p>
                <p><?php echo esc_html( $description ); ?></p>
            </div>
            <?php } 
endwhile; ?>
    </div>
<?php endif; ?>

<?php if( have_rows('maps') ): ?>
    <div class="acf-map group-3" data-zoom="5" style="display: none">
        <?php while ( have_rows('maps') ) : the_row(); 

            // Load sub field values.
            $location = get_sub_field('map_location');
            $title = get_sub_field('location_text');
            $description = get_sub_field('description');
            $group = get_sub_field('group');
            if($group == 3) {
            ?>
            <div  class="marker " data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>">
                <h3><?php echo esc_html( $title ); ?></h3>
                <!-- <p><em><?php echo esc_html( $location['address'] ); ?></em></p> -->
                <!-- <p><?php echo esc_html( $description ); ?></p> -->
            </div>
    <?php 
} endwhile; ?>
    </div>
<?php endif; ?>