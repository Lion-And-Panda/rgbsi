<?php
/* ------------------------------------------------------------------------- *
 * 	Butler 
 *  Resource Library Process		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
    require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    global $wpdb;    
    
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    
    /**********************
    ****** GET ARGS *******
    **********************/   
    
    $resource_categories    = isset($_POST['resource_categories'])? $_POST['resource_categories'] : false;
    $groups                 = isset($_POST['groups'])? $_POST['groups'] : false;
    $types                  = isset($_POST['type_group'])? $_POST['type_group'] : false;
    $tags                   = isset($_POST['tags'])? $_POST['tags'] : false;
    $tagSearchString        = isset($_POST['tagSearch'])? $_POST['tagSearch'] : false;
    $titleSearchString      = isset($_POST['titleSearch'])? $_POST['titleSearch'] : false;

    $args = [];
    $taxCount = 0; //track the number of taxonomy query values

    /********* LIBRARY TYPES *********/

    $itemTerms = [];

    if($resource_categories){
        //if user wants all categories, then simply don't apply this
        if($resource_categories[0] <> "all"){
            //loop through each term, and add it to the search
            if(count($resource_categories) > 1){
                $resourceSearch['relation'] = 'OR';
            }

            $i = 0;
            foreach($resource_categories as $item){
                $itemTerms['taxonomy'] = 'resource_category';
                $itemTerms['field'] = 'slug';
                $itemTerms['terms'] = $item;               

                $resourceSearch[$i] = $itemTerms;
                $i++;
            }
            $taxCount++;            
        }
    }      

    /********* GROUPS *********/    

    $itemTerms = [];

    if($groups){
        //loop through each term, and add it to the search
        if(count($groups) > 1){
            $groupSearch['relation'] = 'OR';
        }

        $i = 0;
        foreach($groups as $item){
            $itemTerms['taxonomy'] = 'product_group';
            $itemTerms['field'] = 'slug';
            $itemTerms['terms'] = $item;               

            $groupSearch[$i] = $itemTerms;
            $i++;
        }
        $taxCount++;
    }        

    /********* TYPES *********/

    $itemTerms = [];

    if($types){
        if($types[0] <> "all"){
            //loop through each term, and add it to the search
            if(count($types) > 1){
                $typeSearch['relation'] = 'OR';
            }
            $i = 0;
            foreach($types as $item){
                $itemTerms['taxonomy'] = 'type_group';
                $itemTerms['field'] = 'slug';
                $itemTerms['terms'] = $item;

                $typeSearch[$i] = $itemTerms;
                $i++;
            }
            $taxCount++;
        }
    }          

    /********* TAGS *********/

    $itemTerms = [];

    if($tags){
        //loop through each term, and add it to the search
        if(count($tags) > 1){
            $tagSearch['relation'] = 'OR';
        }

        $i = 0;
        foreach($tags as $item){
            $itemTerms['taxonomy'] = 'post_tag';
            $itemTerms['field'] = 'slug';
            $itemTerms['terms'] = $item;

            $tagSearch[$i] = $itemTerms;
            $i++;
        }
        $taxCount++;
    }    

    /********* COMBINE FILTERS *********/

    if($taxCount > 1){
        $args['tax_query']['relation'] = 'AND';
        $i = 0;
        //only apply resources, if there is specific criteria to search for
        if(($resource_categories) && ($resource_categories[0] <> "all")){
            $args['tax_query'][$i] = $resourceSearch;
            $i++;
        }        
        if($groups){
            $args['tax_query'][$i] = $groupSearch;
            $i++;
        }
        if($types){
            $args['tax_query'][$i] = $typeSearch;
            $i++;
        }
        if($tags){
            $args['tax_query'][$i] = $tagSearch;
            $i++;
        }

    } elseif(($resource_categories) && ($resource_categories[0] <> "all")){
        $args['tax_query'] = $resourceSearch;
    } elseif($groups){
        $args['tax_query'] = $groupSearch;
    } elseif($types){
        if($types[0] <> "all"){
            $args['tax_query'] = $typeSearch;
        }
    } elseif($tags){
        $args['tax_query'] = $tagSearch;
    }

    //global query values
    if($titleSearchString){
		$args['s'] = $titleSearchString;
		// ga track search term.
    }

    $args['post_type'] = 'resource';
    $args['posts_per_page'] = -1;
    $args['orderby'] = 'menu_order title';
	$args['order'] = 'ASC';
	
 
    
    if($_POST['pullType'] == "results"){

        /***************************
        ****** SUBMIT FILTER *******
        ***************************/    
        ?>
<div class="columns is-multiline  ">
<?php
$searchLoop = new WP_Query( $args );                         

// echo "<pre>";
// print_r($searchLoop);
// echo "</pre>";    
$now = new DateTime();
$member_group_terms = get_terms( 'resource_category' );
foreach ( $member_group_terms as $member_group_term ) { ?>
    <div class="column is-12">
        <h2 class="insight-cat"><?php echo $member_group_term->name; ?></h2>
    </div>

    <?php if($searchLoop->have_posts()){ ?>
    <?php while ( $searchLoop->have_posts() ) : $searchLoop->the_post(); 
    if (has_term($member_group_term->slug, 'resource_category', null) == 1) {
	    if(get_field('file')){
			$searchURL = get_field('file')['url']; ?>
            <div data-type="file" data-id="post-<?php the_ID(); ?>" <?php post_class('column is-6 feed'); ?>>
                <article onclick="window.open('<?= $searchURL; ?>'); ga('<?php the_title(); ?>');" style="cursor: pointer;">
    <?php 
        } elseif(get_field('link')) {
			$searchURL = get_field('link'); ?>
            <div data-type="link" data-id="post-<?php the_ID(); ?>" <?php post_class('column is-6 feed'); ?>>
                <article onclick="window.open('<?= $searchURL; ?>'); ga('<?php the_title(); ?>');" style="cursor: pointer;">
    <?php 
        } elseif(get_field('video')) { 
			$vid = get_field('video', false, false); ?>
            <div data-type="video" data-id="post-<?php the_ID(); ?>" <?php post_class('column is-6 feed'); ?>>
                <article class="popup-youtube" onclick="linkVideo('<?php echo $vid; ?>?rel=0&autoplay=1'); ga('<?php the_title(); ?>');" style="cursor: pointer;">
    <?php 
        } else {
			$searchURL = get_permalink(get_the_ID()); ?>
            <div data-type="regular-default" data-id="post-<?php the_ID(); ?>" <?php post_class('column is-6 feed'); ?>>
                <article onclick="link('<?= $searchURL; ?>'); ga('<?php the_title(); ?>');" style="cursor: pointer;">
    <?php 
        } 
            $terms = get_the_terms(get_the_ID(), 'type_group');
            if($terms){
                $type = $terms[0]->name;
            } else {
                $type = "Info";
            }
						// $post_date = get_the_date( 'F n, Y' );

            $thumbnail_id = get_post_thumbnail_id();
            $thumbnail_url= wp_get_attachment_image_src($thumbnail_id, 'small_thumb', true);
            $thumbnail_meta = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
            $hero = $thumbnail_url[0];
            if ( has_post_thumbnail() ) { ?>
                <div class="blog-feature">
                    <img class="hundred" src="<?= $hero; ?>">
                </div>
            <?php } else { ?>
                <?php if($type == 'Whitepapers' || $type == 'Data Sheets' || $type == 'Downloads' || $type == 'PDF' || $type == 'File') { ?>
                    <div class="blog-feature is-icon downloads">
                        <i class="fas fa-file-alt"></i>
                    </div>
                <?php } elseif($type == 'External Link' || $type == 'Interanal Link') { ?>
                    <div class="blog-feature is-icon external">
                        <i class="fas fa-external-link"></i>
                    </div>
                <?php } elseif($type == 'Videos' || $type == 'Video') { ?>
                    <div class="blog-feature is-icon videos">
                        <i class="fas fa-file-video"></i>
                    </div>
                <?php } else { ?>
                    <div class="blog-feature is-icon info">
                        <i class="fas fa-file-download"></i>
                    </div>
                <?php } ?>
            <?php } ?>

                <div class="blog-feed">


                    <section class="post-section">
                        <div class="entry-content">
                            <h3 class="	"><?php the_title(); ?></h3>
                        </div>
                    </section>

                    <div class="post-footer">
                        <?php if($type == 'Whitepapers' || $type == 'Data Sheets' || $type == 'Downloads' || $type == 'PDF' || $type == 'File') { ?>
                            <a class="button is-primary is-outlined" title="<?php the_title(); ?>"><span>Download
                            </span>
    <span class="icon is-small">
    <i class="fal fa-long-arrow-down"></i>
    </span></a>
                        <?php } elseif($type == 'External Link' || $type == 'Interanal Link') { ?>
                            <a class="button is-primary is-outlined" title="<?php the_title(); ?>"><span>View</span>
    <span class="icon is-small">
    <i class="fal fa-long-arrow-right"></i>
    </span></a>
                        <?php } elseif($type == 'Videos' || $type == 'Video') { ?>
                            <a class="button is-primary is-outlined" title="<?php the_title(); ?>"><span>Watch</span>
    <span class="icon is-small">
    <i class="fal fa-long-arrow-right"></i>
    </span></a>
                        <?php } else { ?>
                            <a class="button is-primary is-outlined" title="<?php the_title(); ?>"><span>Download</span>
    <span class="icon is-small">
    <i class="fal fa-long-arrow-down"></i>
    </span></a>
                        <?php } ?>
                    </div>
                </div>
            </article>
        </div>

        <?php 
        }
		endwhile; 
		wp_reset_query(); 
		wp_reset_postdata(); ?>
    <?php } else { ?>
        <div class="text-center">No Insights match your search criteria. Please try changing the filters.</div>
    <?php } ?>
    <div class="column is-12">
        <hr>
    </div>

<?php
                    
    // Reset things, for good measure
    $member_group_query = null;
    wp_reset_postdata();
}
?>
</div>
<?php } else if($_POST['pullType'] == "updateTags"){

		/*************************
		****** UPDATE TAGS *******
		*************************/      
	
		$newTags = getTags($args, $tagSearchString);
		$sliced_tags = array_slice($newTags,0,11);

		if($sliced_tags){
			foreach($sliced_tags as $tag){
				if($tag->count > 0){
					if($tags) {
						$checkedString = (array_search($tag->slug, $tags) > -1 ? 'checked="checked"' : '');
					} else {
						$checkedString = '';
					}
					?>
                    <div class="item">
                        <input class="filter with-font tagFilter" type="checkbox" name="tags[]" id="<?= $tag->slug; ?>"
                            value="<?= $tag->slug; ?>" data-count="<?= $tag->count; ?>" <?= $checkedString; ?>>
                        <label for="<?= $tag->slug; ?>"><?= truncate($tag->name); ?></label>
                    </div>
                    <?php }
			}
		} else { ?>
                    <div class="item">No tags found; please try a different search.</div>
                    <?php } ?>
                    <!-- <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js'></script> -->

                    
                    <!-- <input id="tagSearch" class="right" type="search" placeholder="Search" value="<?php //echo $_POST['tagValue']; ?>"/> -->
                    <?php 
    } else if($_POST['pullType'] == "updateSidebar"){

		/****************************
		****** UPDATE SIDEBAR *******
		****************************/  

		$newTags = getTags($args, $tagSearchString);
	
		
		$sliced_tags = array_slice($newTags,0,6);
		
		if($sliced_tags){
			foreach($sliced_tags as $tag){
				//if tag has count, and tag isn't already selected in the filter, THEN display in sidebar
				if($tags) {
					if ($tag->count > 0 && !(array_search($tag->slug, $tags) > -1)) { ?>
                    <li class="sidebarFilter" id="<?= $tag->slug; ?>-sidebar" data-count="<?= $tag->count; ?>">
                        <a><?= $tag->name; ?></a></li>
                    <?php }
				} else { ?>
                    <li class="sidebarFilter" id="<?= $tag->slug; ?>-sidebar" data-count="<?= $tag->count; ?>" <a>
                        <?= $tag->name; ?></a></li>
                    <?php
				}
			}
		}
    }
    
    /***************************
    ***** DATABASE LOGGING *****
    ***************************/

    //only run when the search results are displayed, to avoid mis-counting the tag/sidebar updates
    // if($_POST['pullType'] == "results"){
    //     require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
    //     require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    //     global $wpdb;

    //     $pageName = "Resource Library";
    //     $values = '{"resource_categories": "'. ( $resource_categories ? implode(", ", $resource_categories) : "" ) .'", "groups": "'. ( $groups ? implode(", ", $groups) : "" ) .'", "types": "'.( $types ? implode(", ", $types) : "" ) .'", "tags": "'. ( $tags ? implode(", ", $tags) : "" ) .'", "titleSearch": "'. ( $titleSearchString ? $titleSearchString : "" ) .'", "tagSearch": "'. ( $tagSearchString ? $tagSearchString : "" ) .'"}';

    // }   
    
//get the tags for the searched posts
function getTags( $args, $tagSearchString ) {
    $searchLoop = new WP_Query( $args );                         

    $searchTags = [];

    if($searchLoop->have_posts()){
        while ( $searchLoop->have_posts() ) : $searchLoop->the_post();        
            $tags = wp_get_post_tags(get_the_ID());

            foreach ($tags as $tag){
                if(!isInMultiArray($searchTags, "term_id", $tag->term_id )){
                    if($tagSearchString){
                        //if user is searching for a specific tag name, filter by that
                        //echo "name: ".$tag->name." search: ".$tagSearchString." strpos: ".strpos($tag->name, $tagSearchString)."<br>";
                        if(is_numeric(strpos(strtolower($tag->name), strtolower($tagSearchString)))){
                            $searchTags[] = $tag;
                        }
                    } else {
                        $searchTags[] = $tag;
                    }
                }                        
            }

        endwhile; wp_reset_query(); wp_reset_postdata();
    } else { ?>
                    <!--<div class="text-center">No tags match your search criteria.</div>-->
                    <?php }       

    return sort_posts( $searchTags, 'term_id', 'count', $order = 'DESC', $unique = true );
}
    
    
//function used for array validation
function isInMultiArray($array, $field, $position) {
    foreach($array as $index => $item) {
        if($item->$field == $position){
            return TRUE;
        }
    }
    return FALSE;
}     

//used for sorting WP_post object arrays
function sort_posts( $posts, $IDfield, $orderby, $order = 'ASC', $unique = true ) {
	if ( ! is_array( $posts ) ) {
		return false;
	}
	
	usort( $posts, array( new Sort_Posts( $orderby, $order ), 'sort' ) );
	
	// use post ids as the array keys
	if ( $unique && count( $posts ) ) {
		$posts = array_combine( wp_list_pluck( $posts, $IDfield ), $posts );
	}
	
	return $posts;
}

//used for sorting WP_post object arrays
class Sort_Posts {
	var $order, $orderby;
	
	function __construct( $orderby, $order ) {
		$this->orderby = $orderby;
		$this->order = ( 'desc' == strtolower( $order ) ) ? 'DESC' : 'ASC';
	}
	
	function sort( $a, $b ) {
		if ( $a->{$this->orderby} == $b->{$this->orderby} ) {
			return 0;
		}
		
		if ( $a->{$this->orderby} < $b->{$this->orderby} ) {
			return ( 'ASC' == $this->order ) ? -1 : 1;
		} else {
			return ( 'ASC' == $this->order ) ? 1 : -1;
		}
	}
}
?>

<script type="text/javascript">
                    function link(link) {
                        location.href = link;
                    }

                    function linkVideo(vid) {
                        console.log(vid);
                        jQuery.magnificPopup.open({
                            items: {
                                src: vid,
                            },
                            disableOn: 700,
                            type: 'iframe',
                        });
                    }

                    function ga(title) {
                        // ga('send', 'event', 'Web Application', 'Resource', title)
                        gtag('event', 'Resources', {
                            'event_category': 'Resources',
                            'event_label': title,
                            'value': title
                        });

                    }
                    </script>