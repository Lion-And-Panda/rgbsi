// @codekit-prepend "menu/menu.js", "magnific.js";

jQuery(document).ready(function( $ ) {
	//Add Has Children to a UL that has children
	$('ul li:has(ul)').addClass('item-has-children');
	
	// Add smooth scrolling to all links
	$("a").on('click', function(event) {
		if ( $(this).attr('target') == '_blank' ) {
			// do this
			console.log('blank')
		} else {
			console.log('not-blank')
			if(window.location.href == "/") {
				
				if (this.hash !== "") {
					// Prevent default anchor click behavior
					event.preventDefault();
					console.log('no hash')
					// Store hash
					var hash = this.hash;
			
					// Using jQuery's animate() method to add smooth page scroll
					// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 1200, function(){
						window.location.replace("http://www.google.com");
						// Add hash (#) to URL when done scrolling (default click behavior)
						window.location.hash = hash;
					});
				} // End if
			} else {
				console.log('hash')
				var hash = this.hash;
				window.location.replace("/" + hash);
			}
		}
	});

	setTimeout(() => {
		function setleadershipHeight() {
			var AreaHeight = $('.leadership-content').outerHeight();
			$('.leadership-content').css('height' , AreaHeight);
		}
		function setboxHeight() {
			var SurroundingAreaHeight = $('.pb-1').outerHeight();
			$('.partner-relative').css('height' , SurroundingAreaHeight);
		}
		setboxHeight();
		$(window).resize(function() {
			setboxHeight();
			setleadershipHeight();
		});
	}, 500);
	
	$(".pbo-1, .pbo-2,.pbo-3,.pbo-4").on('click', function() {
		$('.partner-box-ovelay').removeClass( "is-active" );
	});

	$('.leadership-click, .leadership-content').click(function() {
		$(this).children('.content').toggleClass('close');
		console.log('click');
	});

	$('.popup-with-move-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,
		
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});

	$(document).ready(function() {
		$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
	
			fixedContentPos: false
		});
	});

});

