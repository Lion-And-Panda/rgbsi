<?php
/*
	Template Name: Services Listing Page
*/
/* ------------------------------------------------------------------------- *
 * 	RGBSI
 *  Services		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
?>

<?php get_header(); ?>
<div id="main" class="content-area relative">
<main id="main-content" class="site-main" role="main">

<?php wp_reset_query(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- Page Content -->

<section id="related" class="section  has-background-dark  cover p-b-100 "
    style="background-image: url(<?php echo esc_url($backgroundR['url']); ?>)">
    <div class="container   ">
        <div class="columns  ">
            <div class="column is-12 ">
                <h1 class="has-text-weight-bold has-text-primary title is-3 p-b-50 p-t-25">Services</h1>
            </div>
        </div>
        <div class="columns is-multiline is-variable">
            <?php 
            $post_objects = get_field('related_capabilities');

            if( $post_objects ): ?>
            <?php foreach( $post_objects as $post):   ?>
                <?php setup_postdata($post); ?>
                <div class="column is-4 content">
                    <article>
                    <?php
                    $thumbnail_id = get_post_thumbnail_id();
                    $thumbnail_url= wp_get_attachment_image_src($thumbnail_id, 'small_thumb', true);
                    $thumbnail_meta = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                    $hero = $thumbnail_url[0];
                    if ( has_post_thumbnail() ) { ?>
                        <div class="featured">
                            <img class="hundred" src="<?= $hero; ?>">
                        </div>
                    <?php }  else if(get_field('header_image')) { 
                        $header_image = get_field('header_image');
                        $size = 'small_thumb';
                        $thumb = $header_image['sizes'][ $size ];
                        ?>
                        <div class="featured">
                            <img class="hundred" src="<?= $thumb; ?>">
                        </div>
                    <?php } ?>
                        <div class="feature-copy">
                            <h3 class="	"><?php the_title(); ?></h3>
                            <?php the_excerpt() ?>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                            viewBox="0 0 44.114 31.562">
                            <g id="Group_32" data-name="Group 32" transform="translate(-432.631 -931.062)">
                                <path id="Path_1" data-name="Path 1" d="M433.632,946.609h41.7L461.2,932.475"
                                    transform="translate(0 0)" fill="none" stroke-linecap="round"
                                    stroke-linejoin="round" stroke-width="2" />
                                <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                    transform="translate(8.111 1.141)" fill="none" stroke-linecap="round"
                                    stroke-width="2" />
                            </g>
                        </svg>
                    </article>
                </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata();  ?>
            <?php endif; ?>
                
        </div>
    </div>
</section>

<?php // the_content(); ?>
<!-- Page Content -->
<?php endwhile; ?>
<?php endif; ?>
<style>
    .embed-container { 
        position: relative; 
        padding-bottom: 56.25%;
        overflow: hidden;
        max-width: 100%;
        height: auto;
    } 

    .embed-container iframe,
    .embed-container object,
    .embed-container embed { 
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>
</main>
</div>
<?php get_footer(); ?>