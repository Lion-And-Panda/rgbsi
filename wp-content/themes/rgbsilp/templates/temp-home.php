<?php
/*
	Template Name: Home Page
*/
/* ------------------------------------------------------------------------- *
 * 	RGBSI
 *  Home		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
?>

<?php get_header(); ?>
<div id="main" class="content-area relative">
    <main id="main-content" class="site-main" role="main">

        <?php wp_reset_query(); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <!-- Page Content -->

        <?php
			$box_link = get_field( 'box_link' );
			$box_link = get_field( 'box_link' );
			$box_link_2 = get_field( 'box_link_2' );
			$background = get_field('background');
            
            $insights = get_field('insights');
            $insights_copy = get_field('insights_copy');
            $insight_image = get_field('insight_image');
            $insight_link = get_field('insight_link');
            
		?>
        <section id="hero" class="section has-background-dark relative p-t-100 cover"
            style="background-image: url(<?php echo esc_url($background['url']); ?>)">
            <div class="container hero-1">
                <div class="columns">
                    <div class="column">
                        <h1 class="title is-1 has-text-white has-text-weight-bold	">
                            <span>Activate<span></span></span>
                            <br>
                            <span>Innovate<span></span></span>
                            <br>
                            <span>Integrate<span></span></span>

                        </h1>
                    </div>
                </div>
            </div>
            <div class="container hero-2 m-t-50 m-b-50">
                <div class="columns">
                    <div class="column">


                        <?php if( $box_link ): 
							$link_url = $box_link['url'];
							$link_title = $box_link['title'];
							$link_target = $box_link['target'] ? $box_link['target'] : '_self';
						?>
                        <a class="button is-primary boxy" href="<?php echo esc_url( $link_url ); ?>"
                            target="<?php echo esc_attr( $link_target ); ?>">
                            <?php echo esc_html( $link_title ); ?>
                            <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                viewBox="0 0 44.114 31.562">
                                <g id="Group_32" data-name="Group 32" transform="translate(-432.631 -931.062)">
                                    <path id="Path_1" data-name="Path 1" d="M433.632,946.609h41.7L461.2,932.475"
                                        transform="translate(0 0)" fill="none" stroke-linecap="round"
                                        stroke-linejoin="round" stroke-width="2" />
                                    <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                        transform="translate(8.111 1.141)" fill="none" stroke-linecap="round"
                                        stroke-width="2" />
                                </g>
                            </svg>

                        </a>
                        <?php endif; ?>
                        <?php if( $box_link_2 ): 
							$link_url_2 = $box_link_2['url'];
							$link_title_2 = $box_link_2['title'];
							$link_target_2 = $box_link_2['target'] ? $box_link_2['target'] : '_self';
						?>
                        <a class="button is-dark boxy" href="<?php echo esc_url( $link_url_2 ); ?>"
                            target="<?php echo esc_attr( $link_target_2 ); ?>">
                            <?php echo esc_html( $link_title_2 ); ?>
                            <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                viewBox="0 0 44.114 31.562">
                                <g id="Group_32" data-name="Group 32" transform="translate(-432.631 -931.062)">
                                    <path id="Path_1" data-name="Path 1" d="M433.632,946.609h41.7L461.2,932.475"
                                        transform="translate(0 0)" fill="none" stroke-linecap="round"
                                        stroke-linejoin="round" stroke-width="2" />
                                    <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                        transform="translate(8.111 1.141)" fill="none" stroke-linecap="round"
                                        stroke-width="2" />
                                </g>
                            </svg>
                        </a>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
            <?php if($insights) { ?>
            <div class="container hero-3">
                <div class="columns ">
                    <div class="column is-6 relative is-offset-6 ">
                        <div class="hero-insights">
                            <div class="hi-left cover "
                                style="background-image: url(<?php echo esc_url($insight_image['sizes']['square']); ?>)">

                            </div>
                            <div class="hi-right">
                                <div class="insight-title">INSIGHTS</div>

                                <div class="hi-top">
                                </div>
                                <?php if( $insight_link ): 
									$link_url_insight_link = $insight_link['url'];
									$link_title_insight_link = $insight_link['title'];
									$link_target_insight_link = $insight_link['target'] ? $insight_link['target'] : '_self';
								?>
                                <a class="button is-light boxy" href="<?php echo esc_url( $link_url_insight_link ); ?>"
                                    target="<?php echo esc_attr( $link_target_insight_link ); ?>">
                                    <p class="title has-text-primary has-text-weight-bold is-5"><?= $insights; ?></p>
                                    <p><?= $insights_copy; ?></p>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                        viewBox="0 0 44.114 31.562">
                                        <g id="Group_32" data-name="Group 32" transform="translate(-432.631 -931.062)">
                                            <path id="Path_1" data-name="Path 1" d="M433.632,946.609h41.7L461.2,932.475"
                                                transform="translate(0 0)" fill="none" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                            <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                                transform="translate(8.111 1.141)" fill="none" stroke-linecap="round"
                                                stroke-width="2" />
                                        </g>
                                    </svg>
                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div id="video-bg">
                <video id="vid" class="fullscreen-bg__video" width="100%" height="auto" video="" loop="" muted="">
                    <source src="<?= get_template_directory_uri(); ?>/video/Sketch_02_17_2020.webmhd.webm" type="video/webm">
                    <source src="<?= get_template_directory_uri(); ?>/video/Sketch_02_17_2020.mp4.mp4" type="video/mp4">
                    <source src="<?= get_template_directory_uri(); ?>/video/Sketch_02_17_2020.oggtheora.ogv" type="video/ogg">
                </video>
                <script>
                    document.getElementById('vid').play();
                </script>
            </div>
        </section>


        <?php
			$intro_headline = get_field( 'intro_headline' );
			$intro_copy = get_field( 'intro_copy' );
			$intro_link = get_field( 'intro_link' );
			$intro_image = get_field('intro_image');

		?>
        <section id="intro" class="section has-background-black relative   cover">
            <div class="container hero-1">
                <div class="columns">
                    <div class="column is-6">
                        <div class="intro relative">
                            <h2 class="title is-2 has-text-success has-text-weight-bold	">
                                <?= $intro_headline; ?>
                            </h2>
                            <p class=" m-b-75">
                                <?= $intro_copy; ?>
                            </p>
                            <?php if( $intro_link ): 
								$link_url_intro = $intro_link['url'];
								$link_title_intro = $intro_link['title'];
								$link_target_intro = $intro_link['target'] ? $intro_link['target'] : '_self';
							?>
                            <a class="button text-link is-white " href="<?php echo esc_url( $link_url_intro ); ?>"
                                target="<?php echo esc_attr( $link_target_intro ); ?>">
                                <?php echo esc_html( $link_title_intro ); ?>
                                <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                    viewBox="0 0 44.114 31.562">
                                    <g id="Group_32" data-name="Group 32" transform="translate(-432.631 -931.062)">
                                        <path id="Path_1" data-name="Path 1" d="M433.632,946.609h41.7L461.2,932.475"
                                            transform="translate(0 0)" fill="none" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2" />
                                        <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                            transform="translate(8.111 1.141)" fill="none" stroke-linecap="round"
                                            stroke-width="2" />
                                    </g>
                                </svg>
                            </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
			if( !empty( $intro_image ) ): ?>
            <img src="<?php echo esc_url($intro_image['url']); ?>" alt="<?php echo esc_attr($intro_image['alt']); ?>" />
            <?php endif; ?>
        </section>

        <section id="documents" class="section has-background-black relative p-t-100 p-b-100   cover">
            <div class="container hero-1">
                <div class="columns is-variable">
                    <?php if ( have_rows('documents') ) : ?>
                    <?php while( have_rows('documents') ) : the_row(); 
                        $t = get_sub_field('document_link');
                        $link_url_t= $t['url'];
                        $link_title_t = $t['title'];
                        $link_target_t = $t['target'] ? $t['target'] : '_self';
                    ?>
                    <div class="column">
                        <a href="<?php echo esc_url( $link_url_t ); ?>"
                            target="<?php echo esc_attr( $link_target_t ); ?>">
                            <div class="card p-b-50">
                                <?php if ( get_sub_field('image') ) : $image = get_sub_field('image'); ?>
                                <div class="card-image relative">
                                    <figure class="image is-16by9">
                                        <img src="<?php echo $image['sizes']['small_thumb']; ?>"
                                            alt="<?php echo $image['alt']; ?>" />
                                    </figure>
                                    <?php if( get_sub_field('date') ) : ?>
                                    <div class="date"><?php echo get_sub_field('date'); ?></div>
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>

                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <?php if( get_sub_field('small_title') ) : ?>
                                            <p class="subtitle is-6 m-b-40"><?php echo get_sub_field('small_title'); ?>
                                            </p>
                                            <?php endif; ?>
                                            <?php if( get_sub_field('title') ) : ?>
                                            <p class="title is-5"><?php echo get_sub_field('title'); ?></p>
                                            <?php endif; ?>
                                            <?php if( get_sub_field('description') ) : ?>
                                            <p><?php echo get_sub_field('description'); ?></p>
                                            <?php endif; ?>

                                            <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                                viewBox="0 0 44.114 31.562">
                                                <g id="Group_32" data-name="Group 32"
                                                    transform="translate(-432.631 -931.062)">
                                                    <path id="Path_1" data-name="Path 1"
                                                        d="M433.632,946.609h41.7L461.2,932.475"
                                                        transform="translate(0 0)" fill="none" stroke-linecap="round"
                                                        stroke-linejoin="round" stroke-width="2" />
                                                    <path id="Path_2" data-name="Path 2"
                                                        d="M453.372,960.068l13.848-14.6"
                                                        transform="translate(8.111 1.141)" fill="none"
                                                        stroke-linecap="round" stroke-width="2" />
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>

        <section id="maps" class="section has-background-black relative   cover">
            <div id="accordion" class="clearfix">
                <div class="accordion-tabs">
                    <ul class="tab-header-and-content ">
                        <li  id="1" class=" accordion-toggle-li active">
                            <div class="wrap">
                                <h4 class="question accordion-toggle text open markerToggle mt-1">
                                    North America
                                </h4>
                                <div class="answer clearfix accordion-content">
                                    <?php if( have_rows('maps') ): ?>
                                    <?php while ( have_rows('maps') ) : the_row(); 

                                    // Load sub field values.
                                    $group = get_sub_field('group');
                                    $title = get_sub_field('location_text');
                                    if($group == 1) {
                                    ?>
                                    <div class="location-title">
                                        <h3><?php echo esc_html( $title ); ?></h3>
                                    </div>
                                    <?php } ?>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </li>
                        <li id="2" class=" accordion-toggle-li">
                            <div class="wrap">
                                <h4   class="question accordion-toggle text markerToggle mt-2">
                                    Europe
                                </h4>
                                <div class="answer clearfix accordion-content" >
                                    <?php if( have_rows('maps') ): ?>
                                    <?php while ( have_rows('maps') ) : the_row(); 

                                    // Load sub field values.
                                    $group = get_sub_field('group');
                                    $title = get_sub_field('location_text');
                                    if($group == 2) {
                                    ?>
                                    <div class="location-title">
                                        <h3><?php echo esc_html( $title ); ?></h3>
                                    </div>
                                    <?php } ?>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </li>
                        <li  id="3" class=" accordion-toggle-li">
                            <div class="wrap">
                                <h4 id="3" class="question accordion-toggle text markerToggle mt-3">
                                    Asia
                                </h4>
                                <div class="answer clearfix accordion-content" >
                                    <?php if( have_rows('maps') ): ?>
                                    <?php while ( have_rows('maps') ) : the_row(); 

                                    // Load sub field values.
                                    $group = get_sub_field('group');
                                    $title = get_sub_field('location_text');
                                    if($group == 3) {
                                    ?>
                                    <div class="location-title">
                                        <h3><?php echo esc_html( $title ); ?></h3>
                                    </div>
                                    <?php } ?>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <script type="text/javascript">
                jQuery(document).ready(function($) {
                    // $(".tab-header-and-content").accordion( {
                    //     heightStyle : "fill"
                    // });
                    $('.accordion-toggle-li').each(function() {
                        var $this = $(this);

                        $this.on('click', function() {
                            $('.accordion-toggle-li').removeClass('active');
                            $this.addClass('active');
                        })
                    });

                    $('.accordion-toggle-li').on('click', function() {
                        $('.acf-map').hide();
                        $('.group-' + this.id).show();
                        console.log(this.id)
                    });
                    // $('.accordion-toggle-li').find('.accordion-toggle').click(function() {

                    //     if ($(this).closest('.question').hasClass("open")) {
                    //         $('.answer').slideUp(500);
                    //         $(this).closest('.question').removeClass("open");
                    //         $(this).next().slideUp(500);
                    //     } else {
                    //         $('.question').removeClass("open");
                    //         $('.answer').slideUp(500);
                    //         $(this).closest('.question').addClass('open');
                    //         $(this).next().slideToggle('fast');
                    //     }

                    // });
                    // var tabsHeight = $('.accordion-heading').outerHeight() * $('.accordion-heading').length;
                    // var height = $('.tab-header-and-content').innerHeight() - tabsHeight;

                    // $('.accordion-inner').height(height - 1);
                });
                </script>
            </div>



            <div id="map-container">
                <?php  include(TEMPLATEPATH.'/includes/content/theme/maps.php'); ?>
            </div>
        </section>

        <?php
			$background_image = get_field('background_image');
		?>
        <section id="numbers" class="section has-background-black relative p-t-100 p-b-100   cover"
            style="background-image: url(<?php echo esc_url($background_image['url']); ?>)">
            <div class="container num-1 ">
                <div class="columns is-variable is-centered">
                    <?php 
					if ( have_rows('number_boxes') ) : ?>
                    <?php while( have_rows('number_boxes') ) : the_row(); ?>
                    <div class="column is-5">
                        <div class="num-box relative">
                            <div class="num-box-top relative">
                                <?php if( get_sub_field('top_title') ) : ?>
                                <h4 class="has-text-dark is-uppercase has-text-weight-bold">
                                    <?php echo get_sub_field('top_title'); ?></h4>
                                <?php endif; ?>
                                <?php if( get_sub_field('top_number') ) : ?>
                                <h3 class="has-text-dark is-uppercase has-text-weight-bold secondary-text">
                                    <?php echo get_sub_field('top_number'); ?></h3>
                                <?php endif; ?>
                                <?php if( get_sub_field('top_title') ) : ?>
                                <h4 class="has-text-primary is-uppercase has-text-weight-bold">
                                    <?php echo get_sub_field('top_headline'); ?></h4>
                                <?php endif; ?>
                                <svg xmlns="http://www.w3.org/2000/svg" width="35.344" height="35.344"
                                    viewBox="0 0 35.344 35.344">
                                    <g id="Group_21" data-name="Group 21" transform="translate(-826.5 -6079.5)">
                                        <path id="Path_5" data-name="Path 5" d="M834.573,6080.133v33.344"
                                            transform="translate(9.927 0.367)" fill="none" stroke="#1e99c5"
                                            stroke-linecap="round" stroke-width="2" />
                                        <path id="Path_6" data-name="Path 6" d="M834.573,6080.133v33.344"
                                            transform="translate(6940.977 5261.927) rotate(90)" fill="none"
                                            stroke="#1e99c5" stroke-linecap="round" stroke-width="2" />
                                    </g>
                                </svg>
                                <div class="num-box-bottom ">
                                    <?php if( get_sub_field('bottom_headline') ) : ?>
                                    <h4 class="has-text-white is-uppercase has-text-weight-bold">
                                        <?php echo get_sub_field('bottom_headline'); ?></h4>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('bottom_copy') ) : ?>
                                    <p class="has-text-white "><?php echo get_sub_field('bottom_copy'); ?></p>
                                    <?php endif; ?>
                                    <?php 
									$num_link = get_sub_field('link');
									$link_url_n = $num_link['url'];
									$link_title_n = $num_link['title'];
									$link_target_n = $num_link['target'] ? $num_link['target'] : '_self';
								?>
                                    <a class="button is-dark is-fullwidth" href="<?php echo esc_url( $link_url_n ); ?>"
                                        target="<?php echo esc_attr( $link_target_n ); ?>">
                                        <span>
                                            <?php echo esc_attr($link_title_n); ?>
                                        </span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                            viewBox="0 0 44.114 31.562">
                                            <g id="Group_32" data-name="Group 32"
                                                transform="translate(-432.631 -931.062)">
                                                <path id="Path_1" data-name="Path 1"
                                                    d="M433.632,946.609h41.7L461.2,932.475" transform="translate(0 0)"
                                                    fill="none" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="2" />
                                                <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                                    transform="translate(8.111 1.141)" fill="none"
                                                    stroke-linecap="round" stroke-width="2" />
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>

        <?php
            $capabilities_background = get_field('capabilities_background');
            $capabilities_headline = get_field('capabilities_headline');
		?>
        <section id="capabilities" class="section has-background-black relative p-t-100 p-b-100   cover"
            style="background-image: url(<?php echo esc_url($capabilities_background['url']); ?>)">
            <div class="container ">
                <div class="columns is-variable is-multiline">
                    <div class="is-4 is-offset-2 column">
                        <h2 class="title is-1 has-text-primary has-text-weight-bold	">
                            <?= $capabilities_headline; ?>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="container relative ac ">
                <div class="columns is-centered is-variable is-multiline">
                    <?php if ( have_rows('capabilities') ) : ?>
                    <?php while( have_rows('capabilities') ) : the_row(); 
                        $q = get_sub_field('link');
                        $link_url_q = $q['url'];
                        $link_title_q = $q['title'];
                        $link_target_q = $q['target'] ? $q['target'] : '_self';
                    ?>


                    <div class="column is-4">
                        <a href="<?php echo esc_url( $link_url_q ); ?>"
                            target="<?php echo esc_attr( $link_target_q ); ?>">
                            <div class="card p-b-50">
                                <?php if ( get_sub_field('image') ) : $image = get_sub_field('image'); ?>
                                <div class="card-image relative">
                                    <figure class="image is-16by9">
                                        <img src="<?php echo $image['sizes']['small_thumb']; ?>"
                                            alt="<?php echo $image['alt']; ?>" />
                                    </figure>
                                </div>
                                <?php endif; ?>

                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <?php if( get_sub_field('title') ) : ?>
                                            <p class="title is-5"><?php echo get_sub_field('title'); ?></p>
                                            <?php endif; ?>
                                            <?php if( get_sub_field('description') ) : ?>
                                            <p><?php echo get_sub_field('description'); ?></p>
                                            <?php endif; ?>

                                            <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                                viewBox="0 0 44.114 31.562">
                                                <g id="Group_32" data-name="Group 32"
                                                    transform="translate(-432.631 -931.062)">
                                                    <path id="Path_1" data-name="Path 1"
                                                        d="M433.632,946.609h41.7L461.2,932.475"
                                                        transform="translate(0 0)" fill="none" stroke-linecap="round"
                                                        stroke-linejoin="round" stroke-width="2" />
                                                    <path id="Path_2" data-name="Path 2"
                                                        d="M453.372,960.068l13.848-14.6"
                                                        transform="translate(8.111 1.141)" fill="none"
                                                        stroke-linecap="round" stroke-width="2" />
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>


        <?php
            $partner_headline = get_field('partner_headline');
            $partner_copy = get_field('partner_copy');
            $partner_link = get_field('partner_link');
            $link_url_partner = $partner_link['url'];
            $link_title_partner = $partner_link['title'];
            $link_target_partner = $partner_link['target'] ? $partner_link['target'] : '_self';
        
		?>
        <section id="partner" class="section has-background-white relative p-t-100 p-b-100   cover">
            <div class="container relative  ">
                <div class="columns is-centered is-variable is-vcentered is-multiline">
                    <div class="column is-6">
                        <div>
                            <h2 class="title is-1 has-text-success has-text-weight-bold	">
                                <?= $partner_headline; ?>
                            </h2>
                            <p><?= $partner_copy; ?></p>
                            <a class="button text-link is-white " href="<?php echo esc_url( $link_url_partner ); ?>"
                                target="<?php echo esc_attr( $link_target_partner ); ?>">
                                <?= $link_title_partner; ?>
                                <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                    viewBox="0 0 44.114 31.562">
                                    <g id="Group_32" data-name="Group 32" transform="translate(-432.631 -931.062)">
                                        <path id="Path_1" data-name="Path 1" d="M433.632,946.609h41.7L461.2,932.475"
                                            transform="translate(0 0)" fill="none" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2"></path>
                                        <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                            transform="translate(8.111 1.141)" fill="none" stroke-linecap="round"
                                            stroke-width="2"></path>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="column is-6">
                        <div class="columns is-multiline is-variable relative m-b-40">
                            <div class="partner-relative column is-12 relative">
                                <?php 
                                if ( have_rows('partner_boxes') ) : 
                                $i = 1; ?>
                                <?php while( have_rows('partner_boxes') ) : the_row(); 
                                
                                    if($i < 3) {
                                ?>

                                <div class="boxy partner-box pb-<?= $i; ?>">
                                    <h3><?php the_sub_field('headline'); ?></h3>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="35.344" height="35.344"
                                        viewBox="0 0 35.344 35.344">
                                        <g id="Group_21" data-name="Group 21" transform="translate(-826.5 -6079.5)">
                                            <path id="Path_5" data-name="Path 5" d="M834.573,6080.133v33.344"
                                                transform="translate(9.927 0.367)" fill="none" stroke="#1e99c5"
                                                stroke-linecap="round" stroke-width="2" />
                                            <path id="Path_6" data-name="Path 6" d="M834.573,6080.133v33.344"
                                                transform="translate(6940.977 5261.927) rotate(90)" fill="none"
                                                stroke="#1e99c5" stroke-linecap="round" stroke-width="2" />
                                        </g>
                                    </svg>
                                </div>
                                <?php $i++; 
                                    }
                                endwhile; ?>
                                <?php endif; ?>
                                <?php 
                                if ( have_rows('partner_boxes') ) : 
                                $i = 1; ?>
                                <?php while( have_rows('partner_boxes') ) : the_row(); 
                                if($i < 3) { ?>
                                <div class="partner-box-ovelay pbo-<?= $i; ?>">
                                    <div class="columns">
                                        <div class="column">
                                            <h3><?php the_sub_field('headline'); ?></h3>
                                            <p><?php the_sub_field('copy'); ?></p>
                                        </div>
                                        <div class="column">
                                            <img src="<?php the_sub_field('image'); ?>" />
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    $(".pb-<?= $i ?>").on('click', function() {
                                        $('.pbo-<?= $i ?>').addClass("is-active");
                                    });
                                });
                                </script>
                                <?php $i++; 
                                 }
                                endwhile; ?>
                                <?php endif; ?>
                            </div>

                        </div>

                        <div class="columns is-multiline is-variable relative">
                            <div class="partner-relative column is-12 relative">
                                <?php 
                                if ( have_rows('partner_boxes') ) : 
                                    $x = 1; ?>
                                <?php while( have_rows('partner_boxes') ) : the_row(); 
                                    if($x > 2) {
                                ?>

                                <div class="boxy partner-box pb-<?= $x; ?>">
                                    <h3><?php the_sub_field('headline'); ?></h3>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="35.344" height="35.344"
                                        viewBox="0 0 35.344 35.344">
                                        <g id="Group_21" data-name="Group 21" transform="translate(-826.5 -6079.5)">
                                            <path id="Path_5" data-name="Path 5" d="M834.573,6080.133v33.344"
                                                transform="translate(9.927 0.367)" fill="none" stroke="#1e99c5"
                                                stroke-linecap="round" stroke-width="2" />
                                            <path id="Path_6" data-name="Path 6" d="M834.573,6080.133v33.344"
                                                transform="translate(6940.977 5261.927) rotate(90)" fill="none"
                                                stroke="#1e99c5" stroke-linecap="round" stroke-width="2" />
                                        </g>
                                    </svg>
                                </div>
                                <?php 
                                    } $x++; 
                                endwhile; ?>
                                <?php endif; ?>


                                <?php 
                                    if ( have_rows('partner_boxes') ) : 
                                    $x = 1; ?>
                                <?php while( have_rows('partner_boxes') ) : the_row(); 
                                if($x > 2) { ?>
                                <div class="partner-box-ovelay pbo-<?= $x; ?>">
                                    <div class="columns">
                                        <div class="column">
                                            <h3><?php the_sub_field('headline'); ?></h3>
                                            <p><?php the_sub_field('copy'); ?></p>
                                        </div>
                                        <div class="column">
                                            <img src="<?php the_sub_field('image'); ?>" />
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    $(".pb-<?= $x ?>").on('click', function() {
                                        $('.pbo-<?= $x ?>').addClass("is-active");
                                    });
                                });
                                </script>
                                <?php 
                                        } $x++; 
                                endwhile; ?>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>



        <?php
            $proven_results_headline = get_field('proven_results_headline');
            $proven_results_copy = get_field('proven_results_copy');
		?>
        <section id="proven" class="section has-background-white relative p-t-50 p-b-100   cover">
            <div class="columns is-gapless is-centered is-vcentered is-multiline">
                <div class="column is-8">
                    <div class="proven-results">
                        <h2 class="has-text-weight-bold has-text-success title is-1"><?= $proven_results_headline; ?>
                        </h2>
                        <p class="has-text-white"><?= $proven_results_copy; ?></p>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="proven-results-svg m-l-30">
                        <img src="<?= get_template_directory_uri(); ?>/images/svg/proven-results.svg" />
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="columns">
                    <div class="column is-12">
                        <?php if ( get_field('proven_partnership_image') ) : ?>
                        <img src="<?php the_field('proven_partnership_image'); ?>" alt="Proven Results">
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>


        <?php
            $leadership_headline = get_field('leadership_headline');
            $leadership_copy = get_field('leadership_copy');
		?>
        <section id="leadership" class="section has-background-black relative p-t-100 p-b-100   cover">
            <div class="container">
                <div id="leadership-order" class="columns is-centered is-vcentered is-multiline">
                    <div class="column is-6">
                        <div class="columns is-variable is-centered is-multiline">
                            <?php if ( have_rows('leadership_team') ) : ?>
                            <?php while( have_rows('leadership_team') ) : the_row(); ?>

                            <div class="column is-6">
                                <div class="card has-background-dark">

                                    <?php if ( get_sub_field('image') ) : $image = get_sub_field('image'); ?>
                                    <div class="card-image">
                                        <figure class="image  is-1by1"> <!-- is-4by3 -->
                                            <img src="<?php echo $image['sizes']['square']; ?>"
                                                alt="<?php echo $image['alt']; ?>" />
                                        </figure>
                                    </div>
                                    <?php endif; ?>

                                    <div class="card-content leadership-click">
                                        <div class="media">
                                            <div class="media-content">
                                                <p class="title has-text-white is-5 has-text-weight-bold">
                                                    <?= get_sub_field('name'); ?></p>
                                                <p class="title has-text-white is-6"><?= get_sub_field('position'); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                            viewBox="0 0 44.114 31.562">
                                            <g id="Group_32" data-name="Group 32"
                                                transform="translate(-432.631 -931.062)">
                                                <path id="Path_1" data-name="Path 1"
                                                    d="M433.632,946.609h41.7L461.2,932.475" transform="translate(0 0)"
                                                    fill="none" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="2" />
                                                <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                                    transform="translate(8.111 1.141)" fill="none"
                                                    stroke-linecap="round" stroke-width="2" />
                                            </g>
                                        </svg>
                                        <div class="content close leadership-content has-background-dark">
                                            <p class="title has-text-white is-5 has-text-weight-bold">
                                                <?= get_sub_field('name'); ?></p>
                                            <p class="title has-text-white is-6"><?= get_sub_field('about'); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="leadership-content-mobile has-background-dark content">
                                    <p class="title has-text-white is-5 has-text-weight-bold">
                                        <?= get_sub_field('name'); ?></p>
                                    <p class="title has-text-white is-6"><?= get_sub_field('position'); ?>
                                    </p>
                                    <p class="title has-text-white is-6"><?= get_sub_field('about'); ?></p>
                                </div>
                            </div>

                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="column is-6">
                        <div class="leadership-team has-background-white">
                            <h2 class="has-text-weight-bold has-text-success title is-1"><?= $leadership_headline; ?>
                            </h2>
                            <p class="ld-copy"><?= $leadership_copy; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>






        <?php
            $difference_headline = get_field('difference_headline');
            $difference_copy = get_field('difference_copy');
            $difference_link = get_field('difference_link');
            $link_url_difference_link = $difference_link['url'];
            $link_title_difference_link = $difference_link['title'];
            $link_target_difference_link = $difference_link['target'] ? $difference_link['target'] : '_self';
		?>
        <section id="difference" class="section has-background-white relative p-t-100 p-b-100   cover">
            <div class="container relative  ">
                <div class="columns is-centered is-vcentered is-multiline">
                    <div class="column is-6">
                        <div>
                            <h2 class="title is-1 has-text-success has-text-weight-bold	">
                                <?= $difference_headline; ?>
                            </h2>
                            <p><?= $difference_copy; ?></p>
                            <a class="button text-link is-white "
                                href="<?php echo esc_url( $link_url_difference_link  ); ?>"
                                target="<?php echo esc_attr( $link_target_difference_link  ); ?>">
                                <?= $link_title_difference_link ; ?>
                                <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                                    viewBox="0 0 44.114 31.562">
                                    <g id="Group_32" data-name="Group 32" transform="translate(-432.631 -931.062)">
                                        <path id="Path_1" data-name="Path 1" d="M433.632,946.609h41.7L461.2,932.475"
                                            transform="translate(0 0)" fill="none" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2"></path>
                                        <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                            transform="translate(8.111 1.141)" fill="none" stroke-linecap="round"
                                            stroke-width="2"></path>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="column is-6">
                        <div class="columns is-multiline is-variable relative m-b-40">
                            <?php if ( get_field('difference_image') ) : $difference_image = get_field('difference_image'); ?>
                            <div class="relative difference-img">
                                <img src="<?php echo $difference_image['sizes']['square']; ?>"
                                    alt="<?php echo $difference_image['alt']; ?>" />
                            </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>



        <?php // the_content(); ?>

        <!-- Page Content -->
        <?php endwhile; ?>
        <?php endif; ?>

    </main>
</div>
<?php get_footer(); ?>