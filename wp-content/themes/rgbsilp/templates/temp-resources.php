<?php
/*
	Template Name: Resource Page
*/
/* ------------------------------------------------------------------------- *
 * 	RGBSI
 *  Resource		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
?>

<?php get_header(); ?>
<div id="main" class="content-area relative">
<main id="main-content" class="site-main" role="main">

<?php wp_reset_query(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- Page Content -->
<?php 
    // Everything else
    $large_image_url = null;
    if ( has_post_thumbnail()) {
        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'big-feature'); 
    }
?>
<div id="hero" class="relative section clearfix cover <?php if( $large_image_url) { ?>has-bg-img<?php } ?>" <?php if( $large_image_url) { ?>style="background-image: url(<?= $large_image_url[0]; ?>);" <?php } ?>>
    <div class="container">
        <div class="columns">
            <div class="column">
                <h1 class=" has-text-weight-bold	">
                    <?php the_title(); ?>
                </h1>
            </div>
        </div>
    </div>
    <div class="header-bg "></div>
</div>



<div id="query-filter" class="section-sm white relative clearfix">
    <form method="post" id="filter-criteria">
        <div class="container">
            <nav class="navbar navbar-filter" role="filters" aria-label="filter navigation">

                <div id="navbarInsight" class="navbar-menu">
                    <div class="navbar-start">
                        <a class="navbar-item libraryTypes selected" id="all" name="all" data-value="all">All</a>
                        <?php
                        $terms = get_terms('type_group', array( 'orderby' => 'count', 'order' => 'DESC' ));
                        foreach($terms as $term){
                            if($term->count > 0){ ?>
                                <a class="navbar-item libraryTypes not-selected" id="<?= $term->slug; ?>" data-value="<?= $term->slug; ?>"><?= $term->name; ?></a></li>
                            <?php }
                        }
                        ?>
                    </div>
                </div>
            </nav>
        </div>
            
        <script type="text/javascript">
            jQuery(document).ready(function($){
                var timeoutID = null;

                $('#titleSearch').keyup(function(e) {
                    clearTimeout(timeoutID);
                    timeoutID = setTimeout(() => titleSearch(), 1500);
                });
                
                function titleSearch() {
                    var strInput = $("#titleSearch").val();
                    if(strInput.length > 2){
                        submitSearch();
                        gtag('event', 'Search Resources', {
                            'event_category': 'Search Resources',
                            'event_label': strInput,
                            'value': strInput
                        });
                    }
                }
                
                $('#tagSearch').on('input',function(e){
                //$(document).on('input', '#tagSearch', function () {
                    clearTimeout(timeoutID);
                    timeoutID = setTimeout(() => tagSearch(), 500);
                });
                
                function tagSearch() {
                    var strInput = $("#tagSearch").val();
                    //console.log(strInput);
                    if(strInput.length > 2 || strInput == ""){
                        //requery the tag box
                        var searchFilter = $("#filter-criteria").serializeArray();
                        searchFilter.push({name: "pullType", value: "updateTags"});
                        //searchFilter.push({name: "tagValue", value: strInput});
                        
                        //grab the resource category, so we know what tags to apply
                        $('.libraryTypes').each(function(){
                            //console.log('in lib');
                            if($(this).hasClass('selected')){
                                searchFilter.push({name: "type_group[]", value: $(this).attr('data-value')});
                                //console.log($(this).attr('data-value'));
                            }
                        });
                        
                        $.ajax ({
                            type: "POST",
                            url: "<?= get_template_directory_uri(); ?>/includes/content/theme/resource-library-process.php",
                            data: searchFilter.valueOf(), //,
                            cache: false,
                            global: false,
                            async: false,
                            success: function(html) {
                                    $("#tagDiv").html(html).hide().fadeIn(1250);
                                    //$('#submit-filter').fadeOut(250); $('.equal-h').matchHeight();
                            },
                            error: function (e) {
                                    alert("An application error occured. Please contact your administrator. [Error code: ajax " + e + "]");
                            }
                        });
                        
                        searchFilter = $("#filter-criteria").serializeArray();
                        searchFilter.push({name: "pullType", value: "updateSidebar"});
                        
                        //grab the resource category, so we know what tags to apply
                        $('.libraryTypes').each(function(){
                            //console.log('in lib');
                            if($(this).hasClass('selected')){
                                searchFilter.push({name: "type_group[]", value: $(this).attr('data-value')});
                                //console.log($(this).attr('data-value'));
                            }
                        });
                        
                        $.ajax ({
                            type: "POST",
                            url: "<?= get_template_directory_uri(); ?>/includes/content/theme/resource-library-process.php",
                            data: searchFilter.valueOf(), //,
                            cache: false,
                            global: false,
                            async: false,
                            success: function(html) {
                                    $("#sidebarFilter").empty().append(html).hide().fadeIn(1250);
                                    //$('#submit-filter').fadeOut(250); $('.equal-h').matchHeight();
                            },
                            error: function (e) {
                                    alert("An application error occured. Please contact your administrator. [Error code: ajax " + e + "]");
                            }
                        });
                    }
                }
                
                $(document).on('click', '.tagFilter', function () {
                    if($("#" + $(this).attr('id') + "-sidebar").attr('id')){
                        $("#" + $(this).attr('id') + "-sidebar").remove();
                    } else {
                        $("#sidebarFilter").append('<li class="sidebarFilter" id="' + $(this).attr('id') + '-sidebar" data-count="' + $(this).attr('data-count') + '"><a>' + $("label[for='" + $(this).attr('id') + "']").text() + '</a></li>');
                        //re-order li's
                        $("#sidebarFilter li").sort(sort_li).appendTo('#sidebarFilter');
                        function sort_li(a, b) {
                            return ($(b).data('count')) > ($(a).data('count')) ? 1 : -1;
                        }
                    }
                    submitSearch();
                });
                    
                
                $(document).on('click', '.sidebarFilter', function () {
                    var strTemp = "";
                    strTemp = $(this).attr('id');
                    strTemp = strTemp.replace("-sidebar","");
                    $("#" + strTemp).prop('checked', true);
                    $(this).remove();
                    submitSearch();
                });
                    
                $(document).on('click', '.typeFilter', function () {
                    submitSearch();
                    //tagSearch();
                });
                    
                $(document).on('click', '.groupFilter', function () {
                    submitSearch();
                    //tagSearch();
                });
                    
                $(document).on('click', '.advanced-filter-trigger', function () {
                    if($(this).hasClass("filterHidden")){
                        $("#advanced-filter").addClass("is-visible");
                        $("#try-advanced-filter").removeClass("is-visible");
                        $("#advanced-filter").removeClass("hidden");
                        $("#filterArrow").addClass("fa-angle-down");
                        $("#filterArrow").removeClass("fa-angle-right");
                        $(this).removeClass("filterHidden");
                    } else {
                        $("#advanced-filter").removeClass("is-visible");
                        $("#advanced-filter").addClass("hidden");
                        $("#try-advanced-filter").removeClass("is-visible");
                        $("#filterArrow").removeClass("fa-angle-down");
                        $("#filterArrow").addClass("fa-angle-right");
                        $(this).addClass("filterHidden");
                    }
                });
                    
                $('.libraryTypes').on('click', function(event){
                    event.preventDefault();
                    if($(this).attr('id') == 'all'){								
                        $('#order').removeClass("selected");
                        $('#order').addClass('not-selected');   
                        $('.libraryTypes').removeClass("selected");
                        $('.libraryTypes').addClass('not-selected');                                                         
                        $('#all').addClass("selected");
                        $('#all').removeClass('not-selected');
                        console.log('all');
                        //reset other filters too
                        $('.groupFilter').each(function () {
                            $(this).attr('checked', false);
                        });

                        $('.typeFilter').each(function () {
                            $(this).attr('checked', false);
                        });

                        $('.tagFilter').each(function () {
                            $(this).attr('checked', false);
                        });
                    } else {
                        //only really does this the first time it's clicked
                        if ($("#advanced-filter").is(':not(.is-visible)')) {
                            $("#try-advanced-filter").addClass("is-visible");
                        }
                        $("#filterArrow").addClass("fa-angle-down");
                        $("#filterArrow").removeClass("fa-angle-right");
                        
                        $('.libraryTypes').addClass('not-selected');
                        $('.libraryTypes').removeClass("selected");
                        //if only one non "All" button clicked, don't unselect it
                        var libraryTypes = document.getElementsByClassName('libraryTypes');
                        var selectedCount = 0;
                        $.each( libraryTypes, function( key, value ) {
                            if($('#' + value.id).hasClass('selected')){
                                selectedCount++;
                            }
                        });

                        if($(this).hasClass('not-selected')){
                            $(this).removeClass('not-selected');
                            $(this).addClass("selected");
                        } else {
                            if(selectedCount > 1){
                                $(this).addClass('not-selected');
                                $(this).removeClass("selected"); 
                            }
                        }
                        $('#all').removeClass("selected");
                        $('#all').addClass('not-selected');
                    }
                    
                    submitSearch();
                    // tagSearch();
                });                                                

                function submitSearch() {
                    //TODO doublecheck how this works once a new tagsearch is done
                    $.when(
                        $('#loading').addClass('is-active'),
                        $('#query-results').html(''),
                        $('#loading').fadeIn(1000),
                        console.log("loading")
                    ).done( function () {
                        //get page array
                        var searchFilter = $('#filter-criteria').serializeArray();

                        //get all selected links
                        $('.libraryTypes').each(function () {
                            if ($(this).hasClass('selected')) {
                                searchFilter.push({
                                    name: "type_group[]",
                                    value: $(this).attr('data-value')
                                });
                            }
                        });

                        searchFilter.push({name: "pullType", value: "results"});
                        
                        $('#reset').fadeIn(1250);
                        //changed = $(this).attr("name"),
                        $.ajax({
                            type: "POST",
                            url: "<?= get_template_directory_uri(); ?>/includes/content/theme/resource-library-process.php",
                            data: searchFilter.valueOf(), //,
                            cache: false,
                            global: false,
                            async: false,
                            success: function (html) {
                                $('#loading').removeClass('is-active');
                                $('#loading').fadeOut(1000);
                                $("#query-results").html(html).hide().fadeIn(1250);
                                //$('#submit-filter').fadeOut(250); $('.equal-h').matchHeight();
                                
                            },
                            error: function (e) {
                                alert("An application error occured. Please contact your administrator. [Error code: ajax " + e + "]");
                            }
                        });
                    });
                    return false;
                }


                //TODO no filter on groups or types???

                //populate page, on initial load
                submitSearch();
                //tagSearch();
                
                
            });
        </script>
    </form>
</div>



<div class="section-flat white relative"> <!-- Container for sidebar tags -->
    <div id="query-results"  class="container white clearfix p-t-50 p-b-50">
    
    </div>
</div>



<?php // the_content(); ?>
<!-- Page Content -->
<?php endwhile; ?>
<?php endif; ?>

</main>
</div>
<?php get_footer(); ?>