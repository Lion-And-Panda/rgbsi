<?php
/*
	Template Name: Services Page
*/
/* ------------------------------------------------------------------------- *
 * 	RGBSI
 *  Services		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
?>

<?php get_header(); ?>
<div id="main" class="content-area relative">
<main id="main-content" class="site-main" role="main">

<?php wp_reset_query(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- Page Content -->
<?php
    $background = get_field('header_image');
?>

<section id="hero" class="section hero-services has-background-dark relative p-t-100 cover"
    style="background-image: url(<?php echo esc_url($background['url']); ?>)">
    <div class="container">
        <div class="columns">
            <div class="column is-5 content">
                <div class="service-headline">
                <?php if ( get_field('header_text') ) : ?>
                    <?php echo get_field('header_text'); ?>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="content" class="section has-background-light ">
    <div class="container is-smaller">
        <div class="columns is-vcentered">
            <div class="column is-7 content">
                <?php if ( get_field('headline') ) : ?>
                    <h2 class=" has-text-weight-bold has-text-dark"><?php echo get_field('headline'); ?></h2>
                <?php endif; ?>
                <?php if ( get_field('copy') ) : ?>
                    <p><?php echo get_field('copy'); ?></p>
                <?php endif; ?>
            </div>
            <div class="column is-3 is-offset-2 content">
                <?php if ( get_field('icon__image') ) : $image = get_field('icon__image'); ?>
                
                    <!-- Full size image -->
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                
                <?php endif; ?>
            </div>
        </div>
        <div class="columns is-vcentered">
            <div class="column is-12 content">
                <hr>
            </div>
        </div>
        <div class="columns ">
            <?php if ( have_rows('box') ) : ?>
            <?php while( have_rows('box') ) : the_row(); ?>
                <div class="column">
                    <div class="service-box">
                        <?php if ( get_sub_field('icon') ) : $image = get_sub_field('icon'); ?>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                        <?php endif; ?>
                        <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                        <?php endif; ?>
                        <span><?php echo  get_sub_field('text');?></span>
           
                    </div>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="columns is-vcentered">
            <div class="column is-12 content">
                <hr>
            </div>
        </div>
        <div class="columns is-vcentered">
            <div class="column is-4 content">
                <?php if ( get_field('file_headline') ) : ?>
                    <h2 class=" has-text-weight-bold has-text-dark"><?php echo get_field('file_headline'); ?></h2>
                <?php endif; ?>
                <?php if ( get_field('file_copy') ) : ?>
                    <p><?php echo get_field('file_copy'); ?></p>
                <?php endif; ?>
            </div>
            <div class="column is-8 ">
            <?php
                $post_object = get_field('file');
                if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); 
                    ?>
                    <div class="feature-box">
                        <?php 
                        $downloadF = get_field('file');
                        $thumbnail_id = get_post_thumbnail_id();
                        $thumbnail_url= wp_get_attachment_image_src($thumbnail_id, 'full', true);
                        $thumbnail_meta = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                        $hero = $thumbnail_url[0];
                        if ( has_post_thumbnail() ) { ?>
                            <div class="service-feature">
                                <img class="hundred" src="<?= $hero; ?>">
                            </div>
                        <?php } ?>
    
                        <a class="button is-primary boxy" href="<?= $downloadF['url'] ?>" target="_blank">
                            Download
                            <svg xmlns="http://www.w3.org/2000/svg" width="22.792" height="31.388" viewBox="0 0 22.792 31.388">
                            <g id="Group_1" data-name="Group 1" transform="translate(953.854 -432.632) rotate(90)">
                                <path id="Path_1" data-name="Path 1" d="M433.632,942.3h28.974l-9.82-9.82" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                <path id="Path_2" data-name="Path 2" d="M453.372,955.612l9.622-10.144" transform="translate(-0.389 -3.172)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"/>
                            </g>
                            </svg>

                        </a>
                        <div class="feature-bg"></div>
                    </div>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
               
            </div>
        </div>
    </div>
</section>
<?php 
$post_objects = get_field('add_resources');
if( $post_objects ): ?>
<section id="whitepapers" class="section hero-services has-background-white relative p-t-100 cover">
    <div class="container p-t-50 is-alt">
        <div class="columns is-multiline is-variable">
            <div class="column is-4  ">
                <h3 class="has-text-weight-bold has-text-primary">WHITEPAPERS/ PDF</h3>
            </div>
            
            <?php foreach( $post_objects as $post):   ?>
                <?php setup_postdata($post); ?>
                <div class="column is-4 content">
                    <article>
                    <?php
                    
                    $downloadFile = get_field('file');
                    $thumbnail_id = get_post_thumbnail_id();
                    $thumbnail_url= wp_get_attachment_image_src($thumbnail_id, 'full', true);
                    $thumbnail_meta = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                    $hero = $thumbnail_url[0];
                    if ( has_post_thumbnail() ) { ?>
                        <div class="blog-feature">
                            <img class="hundred" src="<?= $hero; ?>">
                        </div>
                    <?php } ?>
                 
                <div class="blog-feed">
                    <section class="post-section">
                        <div class="entry-content">
                            <h3 class="	"><?php the_title(); ?></h3>
                        </div>
                    </section>

                    <div class="post-footer">
                        <a href="<?= $downloadFile['url'] ?>" target="_blank" class="button is-small is-primary is-outlined" title="<?php the_title(); ?>"><span>Download</span>
                            <span class="icon is-small">
                                <i class="fal fa-long-arrow-down"></i>
                            </span>
                        </a>
                    </div>
                </div>

                    </article>
                </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata();  ?>
            
                
        </div>
    </div>
</section>

<section>
    <div class="container is-alt">
        <div class="columns is-vcentered">
            <div class="column is-12 content">
                <hr>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php 
$post_objects = get_field('add_videos');

if( $post_objects ): ?>
<section id="videos" class="section  has-background-white relative ">
    <div class="container   is-alt">
        <div class="columns is-multiline is-variable">
            <div class="column is-12 ">
                <h3 class="has-text-weight-bold has-text-primary">VIDEOS</h3>
            </div>
            <?php foreach( $post_objects as $post):   ?>

                <?php setup_postdata($post); ?>
                <div class="column is-4 content">
                    <article>
                        <?php $i = 1; 
                        $vid = get_field('video', false, false); ?>
                        <a class="popup-youtube" href="<?= $vid; ?>">

                        <?php
                        
                        $thumbnail_id = get_post_thumbnail_id();
                        $thumbnail_url= wp_get_attachment_image_src($thumbnail_id, 'small_thumb', true);
                        $thumbnail_meta = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                        $hero = $thumbnail_url[0];
                        if ( has_post_thumbnail() ) { ?>
                            <div class="feature">
                                <img class="hundred" src="<?= $hero; ?>">
                            </div>
                        <?php } ?>

                    
                        <h3 class="	"><?php the_title(); ?></h3>
                        <?php the_excerpt() ?>
                        </a>
                    </article>
                </div>
            <?php $i++;
             endforeach; ?>
            <?php wp_reset_postdata();  ?>
            
                
        </div>
    </div>
</section>
<?php endif; ?>

<?php 
$post_objects = get_field('related_capabilities');
if( $post_objects ): ?>
<?php
    $backgroundR = get_field('related_bg');
?>
<section id="related" class="section  has-background-dark  cover p-b-100 p-t-100"
    style="background-image: url(<?php echo esc_url($backgroundR['url']); ?>)">
    <div class="container   ">
        <div class="columns  ">
            <div class="column is-12 ">
                <h2 class="has-text-weight-bold has-text-primary title is-3  p-b-50 p-t-25">Related Capabilities</h2>
            </div>
        </div>
        <div class="columns is-multiline is-variable">
            
            <?php foreach( $post_objects as $post):   ?>
                <?php setup_postdata($post); ?>
                <div class="column is-4 content">
                    <a href="<?php the_permalink(); ?>">
                    <article>
                    <?php
                    $thumbnail_id = get_post_thumbnail_id();
                    $thumbnail_url= wp_get_attachment_image_src($thumbnail_id, 'small_thumb', true);
                    $thumbnail_meta = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                    $hero = $thumbnail_url[0];
                    if ( has_post_thumbnail() ) { ?>
                        <div class="featured">
                            <img class="hundred" src="<?= $hero; ?>">
                        </div>
                    <?php }  else if(get_field('header_image')) { 
                        $header_image = get_field('header_image');
                        $size = 'small_thumb';
                        $thumb = $header_image['sizes'][ $size ];
                        ?>
                        <div class="featured">
                            <img class="hundred" src="<?= $thumb; ?>">
                        </div>
                    <?php } ?>
                        <div class="feature-copy">
                            <h3 class="	"><?php the_title(); ?></h3>
                            <?php the_excerpt() ?>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="44.114" height="31.562"
                            viewBox="0 0 44.114 31.562">
                            <g id="Group_32" data-name="Group 32" transform="translate(-432.631 -931.062)">
                                <path id="Path_1" data-name="Path 1" d="M433.632,946.609h41.7L461.2,932.475"
                                    transform="translate(0 0)" fill="none" stroke-linecap="round"
                                    stroke-linejoin="round" stroke-width="2" />
                                <path id="Path_2" data-name="Path 2" d="M453.372,960.068l13.848-14.6"
                                    transform="translate(8.111 1.141)" fill="none" stroke-linecap="round"
                                    stroke-width="2" />
                            </g>
                        </svg>
                    </article>
                    </a>
                </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata();  ?>
            
                
        </div>
    </div>
</section>
<?php endif; ?>
<?php // the_content(); ?>
<!-- Page Content -->
<?php endwhile; ?>
<?php endif; ?>
<style>
    .embed-container { 
        position: relative; 
        padding-bottom: 56.25%;
        overflow: hidden;
        max-width: 100%;
        height: auto;
    } 

    .embed-container iframe,
    .embed-container object,
    .embed-container embed { 
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>
</main>
</div>
<?php get_footer(); ?>