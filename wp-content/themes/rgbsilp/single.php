<?php
/* ------------------------------------------------------------------------- *
 * 	RGBSI
 *  Single		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
?>
<?php get_header(); ?>
<div id="main" class="content-area relative">
    <main id="main-content" class="site-main" role="main">

		<?php wp_reset_query(); ?>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php
            $thumb_id = get_post_thumbnail_id();
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
            $hero = $thumb_url_array[0];
            $data = array();
            $id = get_the_ID();
            $title = get_the_title();
            $url = get_permalink();
            
            // color
            $term_list = wp_get_post_terms($id, 'category', array("fields" => "all"));
            $currentcolor = '';
            foreach($term_list as $term_single) 
            {
                $termid = $term_single->term_id; 
                $colorvalue= get_field('category_color',  'category_' . $termid);
                if($colorvalue != '')  $currentcolor = $colorvalue;
                
            }
        ?>
    
        <div class="section-lg p-t-50">
            <div class="container post event texy relative" style="background: <?= $colorvalue; ?>">
                <div class="event-single-content relative">
                    <div class="columns">
                        <div class="column">
                            <?php the_post_thumbnail(); ?>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <div class="event-single-box  postTitle">
                                <h1><?php the_title(); ?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column"> 
                            <div class="event-description event-single-box">
                                <?php the_content(); ?>

                                <?php if ( get_field('video_link') ) : ?>
                                    <div class="embed-container">
                                        <?php the_field('video_link'); ?>
                                    </div>
                                    <style>
                                        .embed-container { 
                                            position: relative; 
                                            padding-bottom: 56.25%;
                                            overflow: hidden;
                                            max-width: 100%;
                                            height: auto;
                                        } 

                                        .embed-container iframe,
                                        .embed-container object,
                                        .embed-container embed { 
                                            position: absolute;
                                            top: 0;
                                            left: 0;
                                            width: 100%;
                                            height: 100%;
                                        }
                                    </style>                                
                                <?php endif; ?>

                                <?php if ( get_field('link') ) : ?>
                                    <a class="button is-primary" target="_blank" href="<?php echo get_field('link'); ?>">Goto Resource</a>
                                <?php endif; ?>
                                
                                <?php if ( get_field('file') ) : ?>
                                    <a class="button is-primary" target="_blank" href="<?php the_field('file'); ?>" >Download File</a>
                                <?php endif; ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>

        <?php endif; ?>
    </main>
</div>
<?php get_footer(); ?>