<?php
//-----------------------------------  // Butler ACF //-----------------------------------//
add_action('acf/init', 'register_acf_block_types');
function register_acf_block_types() {

	if( function_exists('acf_register_block') ) {

		acf_register_block(array(
				'name'				=> 'hero',
				'title'				=> __('Hero'),
				'description'		=> __('A Full Width Hero Banner'),
				'render_callback'	=> 'my_acf_block_render_callback',
				'align' 			=> 'full',
				'category'			=> 'layout',
				'icon'				=> 'format-image',
				'keywords'			=> array( 'hero' ),
				'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
				'mode' 				=> 'edit',
		));
		acf_register_block(array(
				'name'				=> 'image_copy',
				'title'				=> __('Image Copy'),
				'description'		=> __('A Full Width 2 Column Image & Copy Block'),
				'render_callback'	=> 'my_acf_block_render_callback',
				'align' 			=> 'full',
				'category'			=> 'layout',
				'icon'				=> 'forms',
				'keywords'			=> array( 'image', 'copy', 'two', '2', 'column' ),
				'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
				'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'provider_professional',
			'title'				=> __('Provider / Professional + 2 Column'),
			'description'		=> __('A Full Width Provider / Professional Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'laptop',
			'keywords'			=> array( 'provider', 'professional', 'who', 'help', 'column', 'cta', 'link', 'learn' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		// blog posts
		acf_register_block(array(
			'name'				=> 'latest_news',
			'title'				=> __('Latest News'),
			'description'		=> __('A Full Width Latest News Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'provider', 'professional', 'news', 'blog', 'latest' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'sign_up',
			'title'				=> __('Sign Up'),
			'description'		=> __('A Full Width Sign Up Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'rss',
			'keywords'			=> array( 'mail', 'form', 'email', 'subscribe', 'sign' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'upcomping_events',
			'title'				=> __('Upcoming Events'),
			'description'		=> __('A Full Width Upcoming Events Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'calendar',
			'keywords'			=> array( 'event', 'calendar', 'cal', 'upcoming', 'date' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'videos',
			'title'				=> __('Videos'),
			'description'		=> __('A Full Width Videos Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'video-alt3',
			'keywords'			=> array( 'video', 'videos', 'watch' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'event_calendar',
			'title'				=> __('Event Calendar'),
			'description'		=> __('A Full Width Event Calendar Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'calendar-alt',
			'keywords'			=> array( 'cal', 'calendar', 'full', 'event', 'events' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'emergency',
			'title'				=> __('Emergency Contact'),
			'description'		=> __('A Full Width Emergency Banner'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'warning',
			'keywords'			=> array( 'emergency', 'banner', 'call', 'phone' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'latest_events',
			'title'				=> __('Latest Events'),
			'description'		=> __('A Full Width Latest Events Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'warning',
			'keywords'			=> array( 'events', 'latest' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'contact',
			'title'				=> __('Contact'),
			'description'		=> __('A Contact Form Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'email-alt',
			'keywords'			=> array( 'contact', 'form', 'email' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'jobs',
			'title'				=> __('Job Block'),
			'description'		=> __('A Job Listing Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'clipboard',
			'keywords'			=> array( 'job', 'posting', 'employment', 'jobs' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'accordion',
			'title'				=> __('Accordion Block'),
			'description'		=> __('A According Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'clipboard',
			'keywords'			=> array( 'accordion', 'collapse', 'ul', 'question', 'faq', 'faqs' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'staff_leadership',
			'title'				=> __('Staff & Leadership Block'),
			'description'		=> __('A Staff & Leadership Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'admin-users',
			'keywords'			=> array( 'staff', 'leader', 'leadership', 'team', 'board', 'member' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'newsletter',
			'title'				=> __('Newsletter Block'),
			'description'		=> __('A Newsletter Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'dashicons-email',
			'keywords'			=> array( 'newsletter', 'form', 'mail', 'news', 'constant', 'contact' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'cta',
			'title'				=> __('Call To Action Block'),
			'description'		=> __('A Call To Action Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'admin-links',
			'keywords'			=> array( 'cta', 'call', 'action', 'link', 'call to action', 'url' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));

		acf_register_block(array(
			'name'				=> 'page_links',
			'title'				=> __('Page Links Block'),
			'description'		=> __('Page Link Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'media-default',
			'keywords'			=> array( 'cta', 'link', 'page', 'connect', 'call to action', 'url' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'columns',
			'title'				=> __('Custom Columns Block'),
			'description'		=> __('Custom Columns Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'editor-table',
			'keywords'			=> array( 'column', 'col', 'two', 'three', 'four', 'custom' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'logos',
			'title'				=> __('Logos & Links Block'),
			'description'		=> __('Logos & Links Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'megaphone',
			'keywords'			=> array( 'logo', 'link', 'sponsor', 'company', 'employ', 'employer' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		acf_register_block(array(
			'name'				=> 'callout',
			'title'				=> __('Call Out LG Text Block'),
			'description'		=> __('Call Out LG Text Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'full',
			'category'			=> 'layout',
			'icon'				=> 'megaphone',
			'keywords'			=> array( 'call', 'call out', 'large', 'text', 'header', 'statement' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'edit',
		));
		// acf_register_block(array(
		// 	'name'				=> 'services',
		// 	'title'				=> __('Services'),
		// 	'description'		=> __('A Full Width Services Block'),
		// 	'render_callback'	=> 'my_acf_block_render_callback',
		// 	'align' 			=> 'wide',
		// 	'category'			=> 'layout',
		// 	'icon'				=> 'dashicons-admin-tools',
		// 	'keywords'			=> array( 'service', 'serve', 'work' ),
		// 	'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
		// 	'mode' 				=> 'preview',
		// ));
		acf_register_block(array(
			'name'				=> 'boxes',
			'title'				=> __('Boxes'),
			'description'		=> __('A Full Width Boxes Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'wide',
			'category'			=> 'layout',
			'icon'				=> 'align-center',
			'keywords'			=> array( 'service', 'serve', 'work' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'preview',
		));
		acf_register_block(array(
			'name'				=> 'resources',
			'title'				=> __('Resources'),
			'description'		=> __('A Full Width Resources Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'wide',
			'category'			=> 'layout',
			'icon'				=> 'welcome-add-page',
			'keywords'			=> array( 'resources', 'resource', 'library' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'preview',
		));
		acf_register_block(array(
			'name'				=> 'timeline',
			'title'				=> __('Timeline'),
			'description'		=> __('A Full Width Timeline Block'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'align' 			=> 'wide',
			'category'			=> 'layout',
			'icon'				=> 'excerpt-view',
			'keywords'			=> array( 'timeline', 'how', 'service', 'services', 'individual', 'family', 'chart' ),
			'supports' 			=> array( 'align' => array( 'full', 'wide' ),),
			'mode' 				=> 'preview',
		));

	}
}
// Check if function exists and hook into setup.
//if( function_exists('acf_register_block_type') ) {
//}

// Save ACF Fields Constantly
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
    // update path
    $path = get_stylesheet_directory() . '/admin/acffields';
    // return
    return $path;
}
// Sync ACF Fields on Live
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {
    // remove original path (optional)
    unset($paths[0]);
    // append path
    $paths[] = get_stylesheet_directory() . '/admin/acffields';
    // return
    return $paths;    
}
