<?php
if ( function_exists('register_sidebars') )

register_sidebar(array(
		'name' => 'Main Sidebar',
		'id' => 'sidebar-main',
		'description' => 'Main sidebar on the blog.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div class="text-fade"></div></div>'
));

register_sidebar(array(
		'name' => 'Post Sidebar',
		'id' => 'sidebar-single',
		'description' => 'Single post sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '<div class="text-fade"></div></div>'
));