<?php

add_theme_support( 'align-wide' );
add_theme_support( 'responsive-embeds' );

add_theme_support( 'automatic-feed-links' );

add_filter('widget_text', 'do_shortcode');

add_post_type_support( 'page', 'excerpt' );
