<?php
if(!function_exists('vesst_excerpt')){
    function vesst_excerpt( $text='' )
    {
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]&gt;', $text);
        $excerpt_length = apply_filters('excerpt_length', 55);
        $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
        return wp_trim_words( $text, $excerpt_length, $excerpt_more );
    }
    add_filter('wp_trim_excerpt', 'vesst_excerpt');
}