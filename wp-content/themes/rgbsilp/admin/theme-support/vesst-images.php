<?php
add_theme_support('post-thumbnails');
set_post_thumbnail_size( 825, 510, true );// Default Thumb
set_post_thumbnail_size( 'reg-feature',1200, 500, true ); // reg

add_image_size('small_thumb', '640', '360', true);
add_image_size('reg-feature', '1000', '850', true);
add_image_size('big-feature', '1400', '686', false);
add_image_size('square', '800', '800', true);