<?php
add_theme_support('menus');
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'primary' ),
	'secondary' => __( 'Secondary Menu', 'secondary' ),
    'footer' => __( 'Footer Menu', 'footer' ),
) );