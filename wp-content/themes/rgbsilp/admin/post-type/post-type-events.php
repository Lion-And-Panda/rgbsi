<?php
function vesst_register_event() {

	/**
	 * Post Type: Event.
	 */

	$labels = array(
		"name" 			=> __( "Events"),
		"singular_name" => __( "Event"),
		"menu_name" 	=> __( "Event"),
		"add_new"		=> __( "Add New Event"),
		"add_new_item" 	=> __( "Add New Event"),
		"edit_item" 	=> __( "Edit Event"),
		"new_item" 		=> __( "New Event"),
	);

	$args = array(
		"label" 				=> __( "Event"),
		"labels" 				=> $labels,
		"description" 			=> "",
		"public"				=> true,
		"publicly_queryable" 	=> true,
		"show_ui" 				=> true,
		"show_in_rest" 			=> false,
		"rest_base" 			=> "",
		"has_archive" 			=> false,
		"show_in_menu" 			=> true,
		"exclude_from_search" 	=> false,
		"capability_type" 		=> "post",
		'menu_icon'           	=> 'dashicons-calendar-alt',
		"hierarchical" 			=> true,
		"rewrite" 				=> array(
									"slug" => "events",
									"with_front" => true
								),
		"query_var" 			=> true,
		"supports" 				=> array( "title", "thumbnail", "editor", "revisions"),
		"taxonomies" 			=> array( "event_category" ),
	);

	register_post_type( "event", $args );
}

add_action( 'init', 'vesst_register_event' );

//-----------------------------------  // Populate Choices //-----------------------------------//
function acf_location_field( $field ) {
    $data_from_database = array(
				'AL' => 'Alabama',
				'AK' => 'Alaska',
				'AZ' => 'Arizona',
				'AR' => 'Arkansas',
				'CA' => 'California',
				'CO' => 'Colorado',
				'CT' => 'Connecticut',
				'DE' => 'Delaware',
				'DC' => 'District of Columbia',
				'FL' => 'Florida',
				'GA' => 'Georgia',
				'HI' => 'Hawaii',
				'ID' => 'Idaho',
				'IL' => 'Illinois',
				'IN' => 'Indiana',
				'IA' => 'Iowa',
				'KS' => 'Kansas',
				'KY' => 'Kentucky',
				'LA' => 'Louisiana',
				'ME' => 'Maine',
				'MD' => 'Maryland',
				'MA' => 'Massachusetts',
				'MI' => 'Michigan',
				'MN' => 'Minnesota',
				'MS' => 'Mississippi',
				'MO' => 'Missouri',
				'MT' => 'Montana',
				'NE' => 'Nebraska',
				'NV' => 'Nevada',
				'NH' => 'New Hampshire',
				'NJ' => 'New Jersey',
				'NM' => 'New Mexico',
				'NY' => 'New York',
				'NC' => 'North Carolina',
				'ND' => 'North Dakota',
				'OH' => 'Ohio',
				'OK' => 'Oklahoma',
				'OR' => 'Oregon',
				'PA' => 'Pennsylvania',
				'RI' => 'Rhode Island',
				'SC' => 'South Carolina',
				'SD' => 'South Dakota',
				'TN' => 'Tennessee',
				'TX' => 'Texas',
				'UT' => 'Utah',
				'VT' => 'Vermont',
				'VA' => 'Virginia',
				'WA' => 'Washington',
				'WV' => 'West Virginia',
				'WI' => 'Wisconsin',
				'WY' => 'Wyoming'
			);

    $field['choices'] = array();

    //Loop through whatever data you are using, and assign a key/value
    foreach($data_from_database as $field_key => $field_value) {
        $field['choices'][$field_key] = $field_value;
    }
    return $field;
}
add_filter('acf/load_field/name=location', 'acf_location_field');

function vesst_register_event_tax() {

	/**
	 * Taxonomy: Event Taxonomy
	 */

	$labels = array(
		"name" => __( "Event Category"),
		"singular_name" => __( "Event Category"),
	);

	$args = array(
		"label" => __( "Event Category"),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Event Category",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'event_category', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "event_category", array( "event" ), $args );
}

add_action( 'init', 'vesst_register_event_tax' );

// Start Event ...
// if( function_exists('acf_add_local_field_group') ):

// acf_add_local_field_group(array (
// 	'key' => 'group_5bfd8cb822912',
// 	'title' => 'Events',
// 	'fields' => array (
// 		array (
// 			'key' => 'field_5c0058abfe48c',
// 			'label' => 'Event Days/Times',
// 			'name' => 'event_daystimes',
// 			'type' => 'repeater',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array (
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'collapsed' => '',
// 			'min' => 0,
// 			'max' => 0,
// 			'layout' => 'table',
// 			'button_label' => '',
// 			'sub_fields' => array (
// 				array (
// 					'key' => 'field_5c005957fe490',
// 					'label' => 'Start Time',
// 					'name' => 'start_time',
// 					'type' => 'date_time_picker',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array (
// 						'width' => '50',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'display_format' => 'F j, Y g:i a',
// 					'return_format' => 'Y-m-d H:i:s',
// 					'first_day' => 1,
// 				),
// 				array (
// 					'key' => 'field_5c00592ffe48f',
// 					'label' => 'End Time',
// 					'name' => 'end_time',
// 					'type' => 'date_time_picker',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array (
// 						'width' => '50',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'display_format' => 'F j, Y g:i a',
// 					'return_format' => 'Y-m-d H:i:s',
// 					'first_day' => 1,
// 				),
// 			),
// 		),
// 		array (
// 			'key' => 'field_5c0057d2fe48b',
// 			'label' => 'Description',
// 			'name' => 'description',
// 			'type' => 'wysiwyg',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array (
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'default_value' => '',
// 			'tabs' => 'all',
// 			'toolbar' => 'full',
// 			'media_upload' => 1,
// 			'delay' => 0,
// 		),
// 		array (
// 			'key' => 'field_5c0059defe491',
// 			'label' => 'Cost',
// 			'name' => 'cost',
// 			'type' => 'text',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array (
// 				'width' => '30',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'default_value' => '',
// 			'placeholder' => '',
// 			'prepend' => '',
// 			'append' => '',
// 			'maxlength' => '',
// 		),
// 		array (
// 			'key' => 'field_5c006326e2c60',
// 			'label' => 'Add A Sign Up Form',
// 			'name' => 'add_a_sign_up_form',
// 			'type' => 'gravity_forms_field',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array (
// 				'width' => '70',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'allow_null' => 0,
// 			'allow_multiple' => 0,
// 		),
// 		array (
// 			'key' => 'field_5c005a06fe492',
// 			'label' => 'Details',
// 			'name' => 'details',
// 			'type' => 'repeater',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array (
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'collapsed' => '',
// 			'min' => 0,
// 			'max' => 0,
// 			'layout' => 'table',
// 			'button_label' => '',
// 			'sub_fields' => array (
// 				array (
// 					'key' => 'field_5c005a15fe493',
// 					'label' => 'Detail Headline',
// 					'name' => 'detail_headline',
// 					'type' => 'text',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array (
// 						'width' => '30',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'default_value' => '',
// 					'placeholder' => '',
// 					'prepend' => '',
// 					'append' => '',
// 					'maxlength' => '',
// 				),
// 				array (
// 					'key' => 'field_5c005a3dfe494',
// 					'label' => 'Details',
// 					'name' => 'details',
// 					'type' => 'textarea',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array (
// 						'width' => '70',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'default_value' => '',
// 					'placeholder' => '',
// 					'maxlength' => '',
// 					'rows' => 2,
// 					'new_lines' => '',
// 				),
// 			),
// 		),
// 		array (
// 			'key' => 'field_5c38f26e8a4f9',
// 			'label' => 'Page Feed',
// 			'name' => 'page_feed',
// 			'type' => 'repeater',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array (
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'collapsed' => '',
// 			'min' => 0,
// 			'max' => 0,
// 			'layout' => 'row',
// 			'button_label' => '',
// 			'sub_fields' => array (
// 				array (
// 					'key' => 'field_5c38f2928a4fa',
// 					'label' => 'Tab Headline',
// 					'name' => 'tab_headline',
// 					'type' => 'text',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array (
// 						'width' => '',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'default_value' => '',
// 					'placeholder' => '',
// 					'prepend' => '',
// 					'append' => '',
// 					'maxlength' => '',
// 				),
// 				array (
// 					'key' => 'field_5c38f2b38a4fb',
// 					'label' => 'Select A Page',
// 					'name' => 'select_a_page',
// 					'type' => 'post_object',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array (
// 						'width' => '',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'post_type' => array (
// 						0 => 'page',
// 					),
// 					'taxonomy' => array (
// 					),
// 					'allow_null' => 0,
// 					'multiple' => 0,
// 					'return_format' => 'object',
// 					'ui' => 1,
// 				),
// 			),
// 		),
// 	),
// 	'location' => array (
// 		array (
// 			array (
// 				'param' => 'post_type',
// 				'operator' => '==',
// 				'value' => 'event',
// 			),
// 		),
// 	),
// 	'menu_order' => 0,
// 	'position' => 'normal',
// 	'style' => 'default',
// 	'label_placement' => 'top',
// 	'instruction_placement' => 'label',
// 	'hide_on_screen' => '',
// 	'active' => 1,
// 	'description' => '',
// ));

// endif;








// // Event feed on wp_get_attachment_image_src

// if( function_exists('acf_add_local_field_group') ):

// acf_add_local_field_group(array (
// 	'key' => 'group_5c409a218a790',
// 	'title' => 'Events Feed On Pages',
// 	'fields' => array (
// 		array (
// 			'key' => 'field_5c409a218dba9',
// 			'label' => 'Page Feed',
// 			'name' => 'page_feed',
// 			'type' => 'repeater',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array (
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'collapsed' => '',
// 			'min' => 0,
// 			'max' => 0,
// 			'layout' => 'row',
// 			'button_label' => '',
// 			'sub_fields' => array (
// 				array (
// 					'key' => 'field_5c409a219e994',
// 					'label' => 'Tab Headline',
// 					'name' => 'tab_headline',
// 					'type' => 'text',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array (
// 						'width' => '',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'default_value' => '',
// 					'placeholder' => '',
// 					'prepend' => '',
// 					'append' => '',
// 					'maxlength' => '',
// 				),
// 				array (
// 					'key' => 'field_5c409a219e9a2',
// 					'label' => 'Select A Page',
// 					'name' => 'select_a_page',
// 					'type' => 'post_object',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array (
// 						'width' => '',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'post_type' => array (
// 						0 => 'page',
// 					),
// 					'taxonomy' => array (
// 					),
// 					'allow_null' => 0,
// 					'multiple' => 0,
// 					'return_format' => 'object',
// 					'ui' => 1,
// 				),
// 			),
// 		),
// 	),
// 	'location' => array (
// 		array (
// 			array (
// 				'param' => 'post_type',
// 				'operator' => '==',
// 				'value' => 'page',
// 			),
// 		),
// 	),
// 	'menu_order' => 0,
// 	'position' => 'acf_after_title',
// 	'style' => 'default',
// 	'label_placement' => 'top',
// 	'instruction_placement' => 'label',
// 	'hide_on_screen' => '',
// 	'active' => 1,
// 	'description' => '',
// ));

// endif;


// //-----------------------------------  // Populate Choices //-----------------------------------//
// // add_filter('gform_pre_render', 'populate_times');
// //
// // //Note: when changing drop down values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
// // add_filter( 'gform_pre_validation', 'populate_times' );
// //
// // //Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
// // add_filter( 'gform_admin_pre_render', 'populate_times' );
// //
// // //Note: this will allow for the labels to be used during the submission process in case values are enabled
// // add_filter( 'gform_pre_submission_filter', 'populate_times' );
// //
// // function populate_times( $form ) {
// //
// //     if ( $form['title'] != "Sign Up" ) return $form;
// //
// //     foreach ( $form['fields'] as &$field ) {
// //         if ( $field->type != 'select' || strpos( $field->cssClass, 'time-dropdown' ) === false ) {
// //             continue;
// //         }
// //
// //         // you can add additional parameters here to alter the posts that are retrieved
// //         // more info: http://codex.wordpress.org/Template_Tags/get_posts
// //         $movie_ids = get_posts('fields=ids&posts_per_page=-1&post_status=publish&post_type=movie&order=asc&orderby=title');
// //
// //         // update 'Not listed Here' to whatever you'd like the instructive option to be
// //         $choices = array(array('text' => 'Not listed Here', 'value' => 0 ));
// //
// //         foreach ( $movie_ids as $movie_id ) {
// //             $choices[] = array( 'text' => get_the_title( $movie_id ), 'value' => $movie_id, 'isSelected' => false );
// //         }
// //
// //         $field['choices'] = $choices;
// //     }
// //     return $form;
// //}
