<?php
function vesst_register_directory() {

	/**
	 * Post Type: Directory.
	 */

	$labels = array(
		"name" 			=> __( "Directory"),
		"singular_name" => __( "Directory"),
		"menu_name" 	=> __( "Directory Library"),
		"add_new"		=> __( "Add New Directory"),
		"add_new_item" 	=> __( "Add New Directory"),
		"edit_item" 	=> __( "Edit Directory"),
		"new_item" 		=> __( "New Directory"),
	);

	$args = array(
		"label" 				=> __( "Directory"),
		"labels" 				=> $labels,
		"description" 			=> "",
		"public"				=> true,
		"publicly_queryable" 	=> true,
		"show_ui" 				=> true,
		"show_in_rest" 			=> false,
		"rest_base" 			=> "",
		"has_archive" 			=> false,
		"show_in_menu" 			=> true,
		"exclude_from_search" 	=> false,
		"capability_type" 		=> "post",
		'menu_icon'           	=> 'dashicons-media-document',
		"hierarchical" 			=> false,
		"rewrite" 				=> array( 	
									"slug" => "directory", 
									"with_front" => true 
								),
		"query_var" 			=> true,
		"supports" 				=> array( "title", "revisions", "editor"),
		"taxonomies" 			=> array( "directory_category" ),
	);

	register_post_type( "directory", $args );
}

add_action( 'init', 'vesst_register_directory' );


function vesst_register_tax_directory() {

	/**
	 * Taxonomy: Directory Category.
	 */

	$labels = array(
		"name" => __( "Directory Category"),
		"singular_name" => __( "Directory Category"),
	);

	$args = array(
		"label" => __( "Directory Category"),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Directory Category",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'directory_category', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "directory_category", array( "directory" ), $args );
}

add_action( 'init', 'vesst_register_tax_directory' );
