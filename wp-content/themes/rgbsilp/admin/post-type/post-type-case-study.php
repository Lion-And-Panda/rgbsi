<?php
function limber_register_case_study() {

	/**
	 * Post Type: Case Studies.
	 */

	$labels = array(
		"name" 			=> __( "Case Studies"),
		"singular_name" => __( "Case Study"),
		"menu_name" 	=> __( "Case Study"),
		"add_new"		=> __( "Add New Case Study"),
		"add_new_item" 	=> __( "Add New Case Study"),
		"edit_item" 	=> __( "Edit Case Study"),
		"new_item" 		=> __( "New Case Study"),
	);

	$args = array(
		"label" 				=> __( "Case Study"),
		"labels" 				=> $labels,
		"description" 			=> "",
		"public"				=> true,
		"publicly_queryable" 	=> true,
		"show_ui" 				=> true,
		"show_in_rest" 			=> false,
		"rest_base" 			=> "",
		"has_archive" 			=> false,
		"show_in_menu" 			=> true,
		"exclude_from_search" 	=> false,
		"capability_type" 		=> "post",
		'menu_icon'           	=> 'dashicons-media-document',
		"hierarchical" 			=> false,
		"rewrite" 				=> array( 	
									"slug" => "case-study", 
									"with_front" => true 
								),
		"query_var" 			=> true,
		"supports" 				=> array( "title", "thumbnail", "revisions"),
		"taxonomies" 			=> array( "case_study_category", "category" ),
	);

	register_post_type( "case_study", $args );
}

add_action( 'init', 'limber_register_case_study' );



function limber_register_case_study_tax() {

	/**
	 * Taxonomy: Case Study Taxonomy
	 */

	$labels = array(
		"name" => __( "Case Study Category"),
		"singular_name" => __( "Case Study Category"),
	);

	$args = array(
		"label" => __( "Case Study Category"),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Case Study Category",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'case_study_category', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "case_study_category", array( "case_study" ), $args );
}

add_action( 'init', 'limber_register_case_study_tax' );

if( function_exists('acf_add_local_field_group') ) {

    acf_add_local_field_group(array(
        'key' => 'group_57561cc0d37ac',
        'title' => 'Case Study',
        'fields' => array(
            array(
                'key' => 'field_57561ccafa5c6',
                'label' => 'Copy One',
                'name' => 'copy_one',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
            ),
            array(
                'key' => 'field_57561cfefa5c7',
                'label' => 'Featured Sentence',
                'name' => 'featured_sentence',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array(
                'key' => 'field_57561d18fa5c8',
                'label' => 'Image One',
                'name' => 'image_one',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => 'field_57561d38fa5c9',
                'label' => 'Image Two',
                'name' => 'image_two',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => 'field_57561d42fa5ca',
                'label' => 'Closing Copy',
                'name' => 'closing_copy',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'case_study',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
}
