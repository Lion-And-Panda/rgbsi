<?php
function vesst_register_team() {

	/**
	 * Post Type: Staff & Leadership
	 */

	$labels = array(
		"name" 			=> __( "Staff & Leadership Members"),
		"singular_name" => __( "Staff & Leadership"),
		"menu_name" 	=> __( "Staff & Leadership"),
		"add_new"		=> __( "Add Member"),
		"add_new_item" 	=> __( "Add New Member"),
		"edit_item" 	=> __( "Edit Member"),
		"new_item" 		=> __( "New Member"),
	);

	$args = array(
		"label" 				=> __( "Staff & Leadership"),
		"labels" 				=> $labels,
		"description" 			=> "",
		"public"				=> true,
		"publicly_queryable" 	=> true,
		"show_ui" 				=> true,
		"show_in_rest" 			=> false,
		"rest_base" 			=> "",
		"has_archive" 			=> false,
		"show_in_menu" 			=> true,
		"exclude_from_search" 	=> false,
		"capability_type" 		=> "post",
		'menu_icon'           	=> 'dashicons-video-alt3',
		"hierarchical" 			=> true,
		"rewrite" 				=> array( 	
									"slug" => "staff-leadership", 
									"with_front" => true 
								),
		"query_var" 			=> true,
		"supports" 				=> array( "title", "thumbnail", "editor", "revisions", "page-attributes"),
		"taxonomies" 			=> array( "staff_leadership_category" ),
	);

	register_post_type( "staff-leadership", $args );
}

add_action( 'init', 'vesst_register_team' );



function vesst_register_team_tax() {

	/**
	 * Taxonomy: Staff & Leadership Taxonomy
	 */

	$labels = array(
		"name" => __( "Staff & Leadership Category"),
		"singular_name" => __( "Staff & Leadership Category"),
	);

	$args = array(
		"label" => __( "Staff & Leadership Category"),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Staff & Leadership Category",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'staff_leadership_category', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "staff_leadership_category", array( "staff-leadership" ), $args );
}

add_action( 'init', 'vesst_register_team_tax' );




// <?php
// function vesst_register_team() {

// 	/**
// 	 * Post Type: Staff/Leadership.
// 	 */

// 	$labels = array(
// 		"name" 			=> __( "Staff & Leadership"),
// 		"singular_name" => __( "Staff & Leadership Member"),
// 		"menu_name" 	=> __( "Staff & Leadership"),
// 		"add_new"		=> __( "Add New Staff & Leadership Member"),
// 		"add_new_item" 	=> __( "Add New Staff & Leadership Member"),
// 		"edit_item" 	=> __( "Edit Member"),
// 		"new_item" 		=> __( "New Member"),
// 	);

// 	$args = array(
// 		"label" 				=> __( "Staff & Leadership"),
// 		"labels" 				=> $labels,
// 		"description" 			=> "",
// 		"public"				=> true,
// 		"publicly_queryable" 	=> true,
// 		"show_ui" 				=> true,
// 		"show_in_rest" 			=> false,
// 		"rest_base" 			=> "",
// 		"has_archive" 			=> false,
// 		"show_in_menu" 			=> true,
// 		"exclude_from_search" 	=> false,
// 		"capability_type" 		=> "post",
// 		'menu_icon'           	=> 'dashicons-admin-users',
// 		"hierarchical" 			=> true,
// 		"rewrite" 				=> array(
// 									"slug" => "staff-leadership",
// 									"with_front" => true
// 								),
// 		"query_var" 			=> true,
// 		"supports" 				=> array( "title", "thumbnail", "editor", "revisions", "page-attributes"),
// 		"taxonomies" 			=> array( "staff_leadership_category" ),
// 	);
// 	register_taxonomy( "staff_leadership_category", array( "staff_leadership" ), $args );

// 	register_post_type( "staff_leadership", $args );
// }

// add_action( 'init', 'vesst_register_team' );
