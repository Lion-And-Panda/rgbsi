<?php
function vesst_register_video() {

	/**
	 * Post Type: Video
	 */

	$labels = array(
		"name" 			=> __( "Videos"),
		"singular_name" => __( "Video"),
		"menu_name" 	=> __( "Video"),
		"add_new"		=> __( "Add New Video"),
		"add_new_item" 	=> __( "Add New Video"),
		"edit_item" 	=> __( "Edit Video"),
		"new_item" 		=> __( "New Video"),
	);

	$args = array(
		"label" 				=> __( "Video"),
		"labels" 				=> $labels,
		"description" 			=> "",
		"public"				=> true,
		"publicly_queryable" 	=> true,
		"show_ui" 				=> true,
		"show_in_rest" 			=> false,
		"rest_base" 			=> "",
		"has_archive" 			=> true,
		"show_in_menu" 			=> true,
		"exclude_from_search" 	=> false,
		"capability_type" 		=> "post",
		'menu_icon'           	=> 'dashicons-video-alt3',
		"hierarchical" 			=> false,
		"rewrite" 				=> array( 	
									"slug" => "video", 
									"with_front" => true 
								),
		"query_var" 			=> true,
		"supports" 				=> array( "title", "revisions", "thumbnail", "editor", "excerpt"),
		"taxonomies" 			=> array(   "category" ),
	);

	register_post_type( "video", $args );
}

add_action( 'init', 'vesst_register_video' );



// function vesst_register_video_tax() {

// 	/**
// 	 * Taxonomy: Video Taxonomy
// 	 */

// 	$labels = array(
// 		"name" => __( "Video Category"),
// 		"singular_name" => __( "Video Category"),
// 	);

// 	$args = array(
// 		"label" => __( "Video Category"),
// 		"labels" => $labels,
// 		"public" => true,
// 		"hierarchical" => true,
// 		"label" => "Video Category",
// 		"show_ui" => true,
// 		"show_in_menu" => true,
// 		"show_in_nav_menus" => true,
// 		"query_var" => true,
// 		"rewrite" => array( 'slug' => 'video_category', 'with_front' => true, ),
// 		"show_admin_column" => false,
// 		"show_in_rest" => false,
// 		"rest_base" => "",
// 		"show_in_quick_edit" => false,
// 	);
// 	register_taxonomy( "video_category", array( "video" ), $args );
// }

// add_action( 'init', 'vesst_register_video_tax' );





//Adds ACF fields to Post List
//Add the custom columns to the video post type:
add_filter( 'manage_video_posts_columns', 'set_custom_edit_video_columns' );
function set_custom_edit_video_columns($columns) {
    $columns['video'] = __( 'Featured', 'your_text_domain' );
    return $columns;
}

// Add the data to the custom columns for the video post type:
add_action( 'manage_video_posts_custom_column' , 'custom_video_column', 10, 2 );
function custom_video_column( $column, $post_id ) {
    switch ( $column ) {
        case 'video' :
            $x =  get_post_meta( $post_id , 'featured' , true ); 
            if( $x == '1' ) { echo '<span class="dashicons dashicons-yes-alt"></span>'; } else { echo '<span class="dashicons dashicons-no-alt"></span>'; }
            break;
    }
}