<?php
function vesst_register_services() {

	/**
	 * Post Type: Services.
	 */

	$labels = array(
		"name" 			=> __( "Services"),
		"singular_name" => __( "Service"),
		"menu_name" 	=> __( "Service"),
		"add_new"		=> __( "Add New Service"),
		"add_new_item" 	=> __( "Add New Service"),
		"edit_item" 	=> __( "Edit Service"),
		"new_item" 		=> __( "New Service"),
	);

	$args = array(
		"label" 				=> __( "Service"),
		"labels" 				=> $labels,
		"description" 			=> "",
		"public"				=> true,
		"publicly_queryable" 	=> true,
		"show_ui" 				=> true,
		"show_in_rest" 			=> true,
		"rest_base" 			=> "",
		"has_archive" 			=> false,
		"show_in_menu" 			=> true,
		"exclude_from_search" 	=> false,
		"capability_type" 		=> "page",
		'menu_icon'           	=> 'dashicons-admin-tools',
		"hierarchical" 			=> false,
		"rewrite" 				=> array(
									"slug" => "services",
									"with_front" => true
								),
		"query_var" 			=> true,
		"supports" 				=> array( "title", "thumbnail", "revisions"),
		"taxonomies" 			=> array( "services_category" )
	);

	register_post_type( "services", $args );
}

add_action( 'init', 'vesst_register_services' );



add_action( 'init', 'create_services_hierarchical_taxonomy', 0 );
function create_services_hierarchical_taxonomy() {
	$labels = array(
		'name' => _x( 'Services Categories', 'taxonomy general name' ),
		'singular_name' => _x( 'Service category', 'taxonomy singular name' ),
		'search_items' => __( 'Search Service categories' ),
		'all_items' => __( 'All Service categories' ),
		'parent_item' => __( 'Parent Service' ),
		'parent_item_colon' => __( 'Parent Service:' ),
		'edit_item' => __( 'Edit Service category' ),
		'update_item' => __( 'Update Service category' ),
		'add_new_item' => __( 'Add New Service category' ),
		'new_item_name' => __( 'New Service Category Name' ),
		'menu_name' => __( 'Services Categories' ),
	);
	register_taxonomy('services_category',array('post'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'services_category' )
	));
};