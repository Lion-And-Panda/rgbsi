<?php
function limber_register_testimonial() {

	/**
	 * Post Type: Case Studies.
	 */

	$labels = array(
		"name" 			=> __( "Testimonials"),
		"singular_name" => __( "Testimonial"),
		"menu_name" 	=> __( "Testimonials"),
		"add_new"		=> __( "Add New Testimonial"),
		"add_new_item" 	=> __( "Add New Testimonial"),
		"edit_item" 	=> __( "Edit Testimonial"),
		"new_item" 		=> __( "New Testimonial"),
	);

	$args = array(
		"label" 				=> __( "Testimonial"),
		"labels" 				=> $labels,
		"description" 			=> "",
		"public"				=> true,
		"publicly_queryable" 	=> true,
		"show_ui" 				=> true,
		"show_in_rest" 			=> false,
		"rest_base" 			=> "",
		"has_archive" 			=> false,
		"show_in_menu" 			=> true,
		"exclude_from_search" 	=> false,
		"capability_type" 		=> "post",
		'menu_icon'           	=> 'dashicons-media-document',
		"hierarchical" 			=> true,
		"rewrite" 				=> array( 	
									"slug" => "testimonial",
									"with_front" => true 
								),
		"query_var" 			=> true,
		"supports" 				=> array( "title", "thumbnail", "revisions"),
		"taxonomies" 			=> array( "testimonial_category", "category" ),
	);

	register_post_type( "testimonial", $args );
}

add_action( 'init', 'limber_register_testimonial' );



function limber_register_testimonial_tax() {

	/**
	 * Taxonomy: Case Study Taxonomy
	 */

	$labels = array(
		"name" => __( "Testimonial Category"),
		"singular_name" => __( "Testimonial Category"),
	);

	$args = array(
		"label" => __( "Testimonial Category"),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Testimonial Category",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'testimonial_category', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "testimonial_category", array( "testimonial" ), $args );
}

add_action( 'init', 'limber_register_testimonial_tax' );

if( function_exists('acf_add_local_field_group') ) {

    acf_add_local_field_group(array(
        'key' => 'group_57561ccpebkac',
        'title' => 'Testimonials',
        'fields' => array(
             array (
                'key' => 'field_59a604553e003',
                'label' => 'Testimonials',
                'name' => 'testimonials',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'block',
                'button_label' => '',
                'sub_fields' => array (
                    array (
                        'key' => 'field_59a6046d3e004',
                        'label' => 'Testimonial Short',
                        'name' => 'testimonial_short',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'rows' => 3,
                        'new_lines' => '',
                    ),
                    array (
                        'key' => 'field_59a6047a3e005',
                        'label' => 'Testimonial Long',
                        'name' => 'testimonial_long',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'media_upload' => 1,
                        'delay' => 0,
                    ),
                    array (
                        'key' => 'field_59a604883e006',
                        'label' => 'Testimonial Name',
                        'name' => 'testimonial_name',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array (
                        'key' => 'field_59a604923e007',
                        'label' => 'Testimonial Position/Business',
                        'name' => 'testimonial_positionbusiness',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array (
                        'key' => 'field_59a604a63e008',
                        'label' => 'Testimonial Image',
                        'name' => 'testimonial_image',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array (
                        'key' => 'field_59b3e5d27e122',
                        'label' => 'Feature This Testimony',
                        'name' => 'feature_this_testimony',
                        'type' => 'true_false',
                        'instructions' => 'This will place the testimony on the Homepage.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'default_value' => 0,
                        'ui' => 1,
                        'ui_on_text' => 'Homepage',
                        'ui_off_text' => 'No',
                    ),
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'testimonial',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
}
