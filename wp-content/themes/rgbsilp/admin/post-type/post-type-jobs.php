<?php
function vesst_register_Job() {

	/**
	 * Post Type: Job
	 */

	$labels = array(
		"name" 			=> __( "Jobs"),
		"singular_name" => __( "Job"),
		"menu_name" 	=> __( "Job"),
		"add_new"		=> __( "Add New Job"),
		"add_new_item" 	=> __( "Add New Job"),
		"edit_item" 	=> __( "Edit Job"),
		"new_item" 		=> __( "New Job"),
	);

	$args = array(
		"label" 				=> __( "Job"),
		"labels" 				=> $labels,
		"description" 			=> "",
		"public"				=> true,
		"publicly_queryable" 	=> true,
		"show_ui" 				=> true,
		"show_in_rest" 			=> false,
		"rest_base" 			=> "",
		"has_archive" 			=> false,
		"show_in_menu" 			=> true,
		"exclude_from_search" 	=> false,
		"capability_type" 		=> "post",
		'menu_icon'           	=> 'dashicons-clipboard',
		"hierarchical" 			=> true,
		"rewrite" 				=> array( 	
									"slug" => "job", 
									"with_front" => true 
								),
		"query_var" 			=> true,
		"supports" 				=> array( "title", "revisions", "thumbnail", "excerpt"),
		"taxonomies" 			=> array( "Job_category" ),
	);

	register_post_type( "Job", $args );
}

add_action( 'init', 'vesst_register_Job' );



function vesst_register_Job_tax() {

	/**
	 * Taxonomy: Job Taxonomy
	 */

	$labels = array(
		"name" => __( "Job Category"),
		"singular_name" => __( "Job Category"),
	);

	$args = array(
		"label" => __( "Job Category"),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Job Category",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'Job_category', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "Job_category", array( "Job" ), $args );
}

add_action( 'init', 'vesst_register_Job_tax' );





//Adds ACF fields to Post List
//Add the custom columns to the Job post type:
add_filter( 'manage_Job_posts_columns', 'set_custom_edit_Job_columns' );
function set_custom_edit_Job_columns($columns) {
    $columns['Job'] = __( 'Featured', 'your_text_domain' );
    return $columns;
}

// Add the data to the custom columns for the Job post type:
add_action( 'manage_Job_posts_custom_column' , 'custom_Job_column', 10, 2 );
function custom_Job_column( $column, $post_id ) {
    switch ( $column ) {
        case 'Job' :
            $x =  get_post_meta( $post_id , 'featured' , true ); 
            if( $x == '1' ) { echo '<span class="dashicons dashicons-yes-alt"></span>'; } else { echo '<span class="dashicons dashicons-no-alt"></span>'; }
            break;
    }
}