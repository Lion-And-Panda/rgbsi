<?php
/* ------------------------------------------------------------------------- *
 * 	RGBSI
 *  Header		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>

	<link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/images/favicon.ico" />

	<?php wp_head(); ?>
	<?php 
		$site_title = get_bloginfo( 'name' );
		$tel = get_field( 'telephone', 'option' );
	?>
	</style>
	<script src="https://kit.fontawesome.com/961b83af81.js" crossorigin="anonymous"></script>

</head>
<body <?php body_class(); ?>>
<div id="wrapper" class=""><!-- wrapper -->
	<header class="vesst-auto-hide-header">
		<div class="logo">
			<a href="/">				
				<img alt="<?= $site_title; ?>" src="<?= get_template_directory_uri(); ?>/images/rgbsi.jpg" />
			</a>
		</div>

		<nav class="vesst-primary-nav">
			<a href="#vesst-navigation" class="nav-trigger">
				<span>
					<em aria-hidden="true"></em>
					Menu
				</span>
			</a> <!-- .nav-trigger -->

			<?php wp_nav_menu( array( 'container'=> false, 'theme_location' => 'primary', 'menu_class' => 'header-nav', 'menu_id' => 'vesst-navigation') ); ?>
			<ul>
				<li>
					<a href="tel:<?= $tel; ?>" class="tel-header">☎ <?= $tel; ?></a>
				</li>
			</ul>
		</nav> <!-- .vesst-primary-nav -->
	</header>
	