<?php
/* ------------------------------------------------------------------------- *
 * 	RGBSI
 *  Page		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
?>
<?php get_header(); ?>
	<div id="main" class="content-area relative">
		<main id="main-content" class="site-main" role="main">

			<?php wp_reset_query(); ?>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php 
					if ( is_front_page() && is_home() ) {
					// Default homepage
					
					} elseif ( is_front_page()){
					// Static homepage
					
					} elseif ( is_home()){
					
					// Blog page
					
					} else {
					
					// Everything else
					$large_image_url = null;
					if ( has_post_thumbnail()) {
					$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'big-feature'); ?>
				
				<?php } ?>
				<!-- hero -->
				<div id="hero" class="relative section clearfix cover <?php if( $large_image_url) { ?>has-bg-img<?php } ?>" <?php if( $large_image_url) { ?>style="background-image: url(<?= $large_image_url[0]; ?>);" <?php } ?>>
					<div class="container">
						<div class="columns">
							<div class="column">
								<h1 class="uppercase has-text-white	">
									<?php the_title(); ?>
								</h1>
								<div class="breadcrumb relative has-text-white	">
									<?php the_breadcrumb(); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="header-bg "></div>
				</div>

				<?php } ?>
					<?php the_content(); ?>
				<?php endwhile; ?>

				<?php endif; ?>

		</main>
	</div>
<?php get_footer(); ?>