<?php
/* ------------------------------------------------------------------------- *
 * 	RGBSI
 *  Footer		Version		 1.0.0
/* ------------------------------------------------------------------------- */	
$site_title = get_bloginfo( 'name' );

	// todo replace all text with variables for multilingual

	$site_title = get_bloginfo( 'name' );
	$address = get_field( 'address', 'option' );
	$tel = get_field( 'telephone', 'option' );
	$footer_cta_content = get_field( 'footer_cta_content', 'option' );
	// $footer_cta_form = get_field( 'footer_cta_form', 'option' );
?>

<footer>

	<section id="footer-cta" class="section has-background-dark relative p-t-100 ">
		<div class="columns is-centered">
			<div class="column is-6">
				<?= $footer_cta_content; ?>
				<?php 
                    $form_object = get_field('footer_cta_form', 'option' );
                    gravity_form_enqueue_scripts($form_object['id'], true);
                    gravity_form($form_object['id'], false, true, false, '', true, 1);  
                ?>
			</div>
		</div>
	</section>


	<section id="footer-column-menu" class="section has-background-black relative p-t-100">
		<div class="columns">
			<div class="column footer-1">
				<img alt="<?= $site_title; ?>" src="<?= get_template_directory_uri(); ?>/images/rgbsi-footer.png" />
			</div>
			<div class="column footer-2">
				<?php wp_nav_menu( 
					array( 
						'theme_location' => 'footer', 
						'container' => false, 
						'menu_class' => 'footer-menu'
					) 
				); ?>
			</div>
			<div class="column footer-3">
				<div class="contact-insights footer-contact">
					<h4 class="is-uppercase m-b-20">Contact Us</h4>
					<h5 class="m-b-5">Phone</h5>
					<?= $tel; ?>
					<h5 class="m-t-40 m-b-5">Address</h5>
					<?= $address; ?>
				</div>
			</div>
			<div class="column footer-4">
				<div class="contact-insights footer-insights">
					<!-- <h4 class=" is-uppercase ">Insights</h4> -->
				</div>
			</div>
		</div>
	</section>

	<!-- <section class="section-sm text-center">
		<div class="container">
			<div class="columns">
				<div class="column">
					<p class="copyright">&copy; <?= $site_title; ?> <?php echo date("Y"); ?>
					<a target="_blank" href="https://.lionandpanda.com">Web Development by Liona & Panda</a></p>
				</div>
			</div>
		</div>
	</section> -->
</footer>
<div id="vesst-nav-overlay">
	<!-- OVERLAY FOR NAV -->
</div>
</div> <!-- This is the wrapper DO NOT DELETE. -->

<?php wp_footer(); ?>
</body>
</html>